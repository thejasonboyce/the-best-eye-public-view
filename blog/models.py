from django.db import models
from time import time
from django.contrib.auth.models import User
from django.utils import timezone
from django.shortcuts import render_to_response
from PIL import Image
from PIL import _imaging
from django.conf import settings
from django.core.files.storage import default_storage as storage
from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import SmartResize, Transpose

def get_upload_file_name(instance, filename):
	return settings.UPLOAD_FILE_PATTERN % (str(time()).replace('.','_'), filename)

# Create your models here.

class Article(models.Model):
	subject				= models.CharField(max_length=256)
	body				= models.TextField(blank=True, null=True)
	article_thumbnail 	= ProcessedImageField(
		upload_to=get_upload_file_name,
		processors=[
			Transpose(),
			SmartResize(300,300),
			],
		format = 'JPEG', 
		options={'quality': 60},
		null = True,
		blank = True
		)
	likes 				= models.ManyToManyField(User, related_name='article_likes')
	pubdate 			= models.DateTimeField(default=timezone.now)
	lastedit			= models.DateTimeField(default=timezone.now)
	createdby			= models.ForeignKey(User, related_name='article_writer')


	def __unicode__(self):
		return self.subject

	class Meta:
		ordering = ['-pubdate']