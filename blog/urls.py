from django.conf.urls import patterns, include, url

urlpatterns = patterns('blog.views',
	url(r'^$', 'articles_all'),
	url(r'^(?P<article_id>\d+)/$', 'get_article'),
	url(r'^new/$', 'new_article'),
	url(r'^(?P<article_id>\d+)/delete/$', 'delete_article'),
	url(r'^(?P<article_id>\d+)/edit/$', 'edit_article'),
    )
