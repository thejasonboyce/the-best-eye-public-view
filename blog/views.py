from django.shortcuts import render_to_response, redirect, render
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.csrf import csrf_exempt
from django.template import Library
from django.db.models import Q
from django.utils import timezone
from datetime import datetime
from userprofile.models import UserProfile
from django.contrib import messages
from django.conf import settings
import csv
from django.template import RequestContext
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from notification.models import SiteMessage
from blog.models import Article
from blog.forms import ArticleForm

register = Library()

# Create your views here.

def articles_all(request):
	if not request.user.is_authenticated():
		thisuser = None
		s = None
		followers = None
	else:
		thisuser = request.user
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
		followers = UserProfile.objects.filter(following=thisuser).count()
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0

	articles = Article.objects.all()

	args = {'thisuser': thisuser, 'following': following, 'followers': followers, 's': s, 'articles': articles}

	return render(request, "blog.html", args)

def get_article(request, article_id=1):
	if not request.user.is_authenticated():
		thisuser = None
		s = None
		followers = None
		following = None
		editable = None
	else:
		thisuser = request.user
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
		followers = UserProfile.objects.filter(following=thisuser).count()
		following = thisuser.user_object.following.count()
		if thisuser.user_object.blog_writer:
			editable = True
		else:
			editable = None

	if Article.objects.filter(id=article_id).exists():
		article = Article.objects.get(id=article_id)
	else:
		article = None	

	args = {'thisuser': thisuser, 'following': following, 'followers': followers, 's': s, 'article': article, 'editable': editable}

	return render(request, "article.html", args)

@login_required
def new_article(request):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
	
	if UserProfile.objects.filter(user=thisuser, blog_writer=True).exists():
		if request.method == "POST":
			form = ArticleForm(request.POST)
			if form.is_valid():
				a = form.save(commit=False)
				a.createdby = thisuser
				a.save()

				messages.add_message(request, messages.SUCCESS, "Your Article was posted.")
				
				return HttpResponseRedirect('/blog/')
		else:
			form = ArticleForm()
		
		args = {'thisuser': thisuser, 'following': following, 'followers': followers, 's': s}
		args.update(csrf(request))
		
		args['form'] = form
		
		return render(request, 'new_article.html', args)
		
	else:
		return HttpResponseRedirect('/blog/')


@login_required
def delete_article(request, article_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser, blog_writer=True).exists():
		article_to_delete = Article.objects.get(id=article_id)
		article_to_delete.delete()
		messages.add_message(request, settings.DELETE_MESSAGE, "Your article was deleted.")

	return HttpResponseRedirect('/blog/')

@login_required
def edit_article(request, article_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
	if not UserProfile.objects.filter(user=thisuser, blog_writer=True).exists():
		return HttpResponseRedirect('/blog/%s' % article_id)
	else:
		edit_article = Article.objects.get(id=article_id)
		if request.method == "POST":
			form = ArticleForm(request.POST)
			if form.is_valid():
				a = form.save(commit=False)
				a.id = edit_article.id
				a.createdby = edit_article.createdby
				if a.article_thumbnail:
					pass
				else:
					a.article_thumbnail = edit_article.article_thumbnail
				a.save()

				messages.add_message(request, messages.SUCCESS, "Your artile was edited.")

				return HttpResponseRedirect('/blog/%s/' % article_id)

		else:
			form = ArticleForm(instance=edit_article)

		args = {'thisuser': thisuser, 'following': following, 'followers': followers, 's': s, 'edit_article': edit_article}
		args.update(csrf(request))

		args['form'] = form

		return render(request, 'edit_article.html', args)