"""
Django settings for thebesteye project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ''

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['127.0.0.1', 'thebesteye.com', 'www.thebesteye.com', '104.236.51.135', 'm.thebesteye.com']

SITE_ID = 1

ADMINS = (
	('', ''),
)

SEND_BROKEN_LINK_EMAILS = True

MANAGERS = (
	('', ''),
)

# Application definition

INSTALLED_APPS = (
	'django.contrib.admin',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.messages',
	'django.contrib.sites',
	'django.contrib.staticfiles',
	'formtools',
	'allauth',
	'allauth.account',
	'allauth.socialaccount',
	'storages',
	'imagekit',
	'userprofile',
	'game',
	'lobby',
	'eyechat',
	'notification',
	'blog',
	'activity',
	'favorite',
	'leaderboard',
)

DELETE_MESSAGE = 50

MESSAGE_TAGS = {
	DELETE_MESSAGE : 'deleted',
}

MIDDLEWARE_CLASSES = (
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.common.CommonMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'thebesteye.urls'

WSGI_APPLICATION = 'thebesteye.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.postgresql_psycopg2',
		'NAME': '',
		'USER': '', 
		'PASSWORD': '',
		'HOST': 'localhost',
		'PORT': '',
	}
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'America/Los_Angeles'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = '/webapps/thebesteye/static/'

STATICFILES_DIRS = (
		('assets', os.path.join(os.getcwd(), 'static/')),
)

MEDIA_ROOT = '/assets/'

TEMPLATE_DIRS = (
	'thebesteye/templates/',
	'userprofile/templates/',
	'game/templates/',
	'lobby/templates/',
	'eyechat/templates/',
	'notification/templates/',
	'blog/templates/',
	'favorite/templates/',
	)

AUTH_PROFILE_MODULE = 'userprofile.UserProfile'

UPLOAD_FILE_PATTERN = "assets/uploaded_files/%s_%s"

AWS_STORAGE_BUCKET_NAME = ''
AWS_ACCESS_KEY_ID = ''
AWS_SECRET_ACCESS_KEY = ''
AWS_PRELOAD_METADATA = True
    
STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
S3_URL = 'http://%s.s3.amazonaws.com/' % AWS_STORAGE_BUCKET_NAME
STATIC_URL = S3_URL

TEMPLATE_CONTEXT_PROCESSORS = {
	"django.contrib.auth.context_processors.auth",
	"django.core.context_processors.debug",
	"django.core.context_processors.i18n",
	"django.core.context_processors.media",
	"django.core.context_processors.static",
	"django.core.context_processors.tz",
	"django.contrib.messages.context_processors.messages",
	"django.core.context_processors.request",
	"allauth.account.context_processors.account",
	"allauth.socialaccount.context_processors.socialaccount",
	}

AUTHENTICATION_BACKENDS = (

	# Needed to login by username in Django admin, regardless of `allauth`
	'django.contrib.auth.backends.ModelBackend',

	# `allauth` specific authentication methods, such as login by e-mail
	'allauth.account.auth_backends.AuthenticationBackend',

)

LOGGING = {
	'version': 1,
	'disable_existing_loggers': False,
	'formatters': {
		'standard': {
			'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
		},
	},
	'handlers': {
		'default': {
			'level': 'DEBUG',
			'class': 'logging.handlers.RotatingFileHandler',
			'filename': '/logs/mylog.log',
			'maxBytes': 1024*1024*5, # 5 MB
			'backupCount': 5,
			'formatter': 'standard',
			}, 
		'request_handler': {
			'level': 'DEBUG',
			'class': 'logging.handlers.RotatingFileHandler',
			'filename': '/logs/django_request.log',
			'maxBytes': 1024*1024*5, # 5 MB
			'backupCount': 5,
			'formatter': 'standard',
			}
	},
	'loggers': {
		'': {
			'handlers': ['request_handler'],
			'level': 'DEBUG',
			'propagate': True
		},
		'django.request': {
			'handlers': ['request_handler'],
			'level': 'DEBUG',
			'propagate': False
			},
	}
}

ACCOUNT_EMAIL_REQUIRED =True
ACCOUNT_EMAIL_VERIFICATION ="mandatory"
ACCOUNT_SIGNUP_PASSWORD_VERIFICATION =True
ACCOUNT_UNIQUE_EMAIL =True
ACCOUNT_USER_MODEL_USERNAME_FIELD ="username"
ACCOUNT_USER_MODEL_EMAIL_FIELD ="email"
ACCOUNT_USERNAME_REQUIRED =True
ACCOUNT_LOGIN_ON_EMAIL_CONFIRMATION =True

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_PORT = 587
DEFAULT_FROM_EMAIL = ''
SERVER_EMAIL = ''