from django.shortcuts import render_to_response, redirect, render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.core.context_processors import csrf
from django.utils import timezone
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.csrf import csrf_exempt
from django.template import Library
from django.db.models import Q
from django.utils import timezone
from django.conf import settings
from django.contrib import messages
from django.template import RequestContext
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import Library
from django.contrib.auth.decorators import login_required
import csv
from datetime import datetime
from userprofile.models import UserProfile
from activity.models import Action
from notification.models import SiteMessage

register = Library()

# Create your views here.

def leaderboards_all(request):
	if not request.user.is_authenticated():
		thisuser = None 
		followers = None
		following_me = None
		s = None
	else:
		thisuser = request.user 
		followers = UserProfile.objects.filter(following=thisuser).count()
		following_me = thisuser
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')

	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0

	args = {'thisuser': thisuser, 'following': following, 'followers': followers, 's': s}
	args['players_list'] = []

	players_list = UserProfile.objects.filter(visible=True).order_by('-totalpoints')
	paginator = Paginator(players_list, 20)
	next = request.path

	page = request.GET.get('page')
	try:
		player_points_list = paginator.page(page)
	except PageNotAnInteger:
		player_points_list = paginator.page(1)
	except EmptyPage:
		player_points_list = paginator.page(paginator.num_pages)
	args['player_points_list'] = player_points_list

	for f in players_list:
		user_status = (f, UserProfile.objects.filter(user=thisuser, following=f.user).exists())
		args['players_list'].append(user_status)

	return render(request, 'leaderboards_all.html', args)