from django.db import models
from time import time
from django.contrib.auth.models import User
from django.utils import timezone
from django.shortcuts import render_to_response
from PIL import Image
from PIL import _imaging
from django.conf import settings
from django.core.files.storage import default_storage as storage

def get_upload_file_name(instance, filename):
	return settings.UPLOAD_FILE_PATTERN % (str(time()).replace('.','_'), filename)

# Create your models here.

