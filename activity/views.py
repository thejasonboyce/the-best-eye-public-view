from django.shortcuts import render_to_response, redirect, render
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.csrf import csrf_exempt
from django.template import Library
from django.db.models import Q
from django.utils import timezone
from datetime import datetime
from userprofile.models import UserProfile
from django.contrib import messages
from django.conf import settings
import csv
from django.template import RequestContext
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from notification.models import SiteMessage
from activity.models import Action

register = Library()

# Create your views here.

@login_required
def activity_feed(request):
	if not request.user.is_authenticated():
		thisuser = None
		s = None
		following = None
		followers = None
	else:
		thisuser = request.user
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
		following = thisuser.user_object.following.count()
		followers = UserProfile.objects.filter(following=thisuser).count()

	following_list = thisuser.user_object.following.all()

	activity_stream_list = Action.objects.filter(user__in=list(following_list)).order_by('-pubdate')

	paginator = Paginator(activity_stream_list, 20)
	next = request.path

	page = request.GET.get('page')
	try:
		activity_stream = paginator.page(page)
	except PageNotAnInteger:
		activity_stream = paginator.page(1)
	except EmptyPage:
		activity_stream = paginator.page(paginator.num_pages)

	args = {'thisuser': thisuser, 's': s, 'following': following, 'followers': followers, 'following_list': following_list, 'activity_stream': activity_stream}

#	activity_stream = []

#	args = {'thisuser': thisuser, 's': s, 'following': following, 'followers': followers, 'following_list': following_list}
#	args['activity_stream'] = []

#	for f in following_list:
#		user_action = Action.objects.filter(user=f).order_by('-pubdate')
#		args['activity_stream'].append(user_action)

	return render(request, 'activity_stream.html', args)