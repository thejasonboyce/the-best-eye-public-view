from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.conf import settings
from django.db.models import Q
from models import FavoriteGame
from forms import FavoriteGameForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from userprofile.models import UserProfile
from game.models import Game, Result
from notification.models import SiteMessage


# Create your views here.

@login_required
def all_favorites(request):
	thisuser = request.user
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
	if UserProfile.objects.filter(user=thisuser).exists():
		followers = UserProfile.objects.filter(following=thisuser).count()
		following = thisuser.user_object.following.count()
	else:
		followers = None
		following = None
	if UserProfile.objects.filter(user=thisuser).exists():
		all_favoritegames_list = thisuser.user_object.favorite_games.all().order_by('-pubdate')
		paginator = Paginator(all_favoritegames_list, 20)
		next = request.path

		page = request.GET.get('page')
		try:
			all_favoritegames = paginator.page(page)
		except PageNotAnInteger:
			all_favoritegames = paginator.page(1)
		except EmptyPage:
			all_favoritegames = paginator.page(paginator.num_pages)

		for game in all_favoritegames:
			game.played = Result.objects.filter(game=game, player=thisuser).exists()
			game.liked = Game.objects.filter(id=game.id, gamelikes=thisuser).exists()
			if Result.objects.filter(game=game).exists():
				game.latest = Result.objects.filter(game=game).order_by('-pubdate')[0]
	else:
		all_favoritegames = None

	new_game_available = True

	if request.method == "POST":
		search_text = request.POST['search_favorite_games']
		search_game_results = thisuser.user_object.favorite_games.filter(Q(name__contains=search_text) | Q(description__contains=search_text), favorite_game__createdby=thisuser).order_by('-pubdate')
		for game in search_game_results:
			game.played = Result.objects.filter(game=game, player=thisuser).exists()
			game.liked = Game.objects.filter(id=game.id, gamelikes=thisuser).exists()
			if Result.objects.filter(game=game).exists():
				game.latest = Result.objects.filter(game=game).order_by('-pubdate')[0]
		search_sent = True
	else: 
		search_game_results = None
		search_sent = False

	args = {'thisuser': thisuser, 'all_favoritegames': all_favoritegames, 'followers': followers, 'following': following, 'search_game_results': search_game_results, 'search_sent': search_sent, 's': s, 'new_game_available': new_game_available}

	return render(request, "all_favoritegames.html", args)

@login_required
def get_favoritegame(request, favoritegame_id=1):
	thisuser = request.user
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
	if FavoriteGame.objects.filter(id=favoritegame_id, createdby=thisuser).exists():
		favorite_game = FavoriteGame.objects.filter(id=bookmark_id, createdby=thisuser).order_by('-pubdate')[0]
	else:
		favorite_game = None

	args = {'thisuser': thisuser, 'favorite_game': favorite_game, 's': s}

	return render(request, "get_favoritegame.html", args)

@login_required
def delete_favoritegame(request, favoritegame_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(favorite_games__id=favoritegame_id, user=thisuser).exists():
		user_account = UserProfile.objects.get(user=thisuser)
		game_to_delete = Game.objects.get(id=favoritegame_id)
		user_account.favorite_games.remove(game_to_delete)

		messages.add_message(request,
							settings.DELETE_MESSAGE,
							"Game was deleted from your favorites.")

		next = request.GET.get('next', None)
		if next:
			return redirect(next)
		else:
			return HttpResponseRedirect('/')

	else:
		return HttpResponseRedirect('/')
