from django.conf.urls import patterns, url

urlpatterns = patterns('favorite.views',
	url(r'^$', 'all_favorites'),
	url(r'^(?P<favoritegame_id>\d+)/$', 'get_favoritegame'),
	url(r'^(?P<favoritegame_id>\d+)/delete/$', 'delete_favoritegame'),
)