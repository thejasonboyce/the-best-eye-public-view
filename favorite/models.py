from django.db import models
from time import time
from django.contrib.auth.models import User
from django.utils import timezone
from django.shortcuts import render_to_response
from PIL import Image
from PIL import _imaging
from django.conf import settings
from django.core.files.storage import default_storage as storage
from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import SmartResize, Transpose
from game.models import Game

def get_upload_file_name(instance, filename):
	return settings.UPLOAD_FILE_PATTERN % (str(time()).replace('.','_'), filename)

# Create your models here.

class FavoriteGame(models.Model):
	title				= models.CharField(max_length=200, blank=True, null=True)
	game 				= models.ForeignKey(Game, related_name='favorite_game')
	createdby			= models.ForeignKey(User, related_name='game_favoriter')
	pubdate				= models.DateTimeField(default=timezone.now)
	
	def __unicode__(self):
		return self.game.name

	class Meta:
		ordering = ['-pubdate']