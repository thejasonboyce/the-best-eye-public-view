from django import forms
from models import FavoriteGame

class FavoriteGameForm(forms.ModelForm):

	class Meta:
		model = FavoriteGame
		fields = ('title',)