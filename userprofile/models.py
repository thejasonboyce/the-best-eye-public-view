from django.db import models
from django.contrib.auth.models import User
from PIL import Image
from PIL import _imaging
from django.conf import settings
from django.core.files.storage import default_storage as storage
from django.utils import timezone
from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import SmartResize, Transpose
from time import time
from game.models import Game, Achievement, Result

def get_upload_file_name(instance, filename):
	return settings.UPLOAD_FILE_PATTERN % (str(time()).replace('.','_'), filename)

# Create your models here.

class UserProfile(models.Model):
	user = models.OneToOneField(User, related_name='user_object')
	visible = models.BooleanField(default=True)
	camera = models.CharField(max_length=100, null=True, blank=True)
	bio = models.TextField(null=True, blank=True)
	avatar = models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	avatar_thumbnail = ProcessedImageField(
		upload_to=get_upload_file_name,
		processors=[
			Transpose(),
			SmartResize(300,300),
			],
		format = 'JPEG', 
		options={'quality': 60},
		null = True,
		blank = True
		)
	totalpoints = models.IntegerField(default=1)
	following = models.ManyToManyField(User, related_name='following_list')
	birthday = models.DateField(null=True, blank=True)
	achievements = models.ManyToManyField(Achievement, related_name='achievements_list')
	blog_writer = models.BooleanField(default=False)
	favorite_games = models.ManyToManyField(Game, related_name='favorite_games')

# 	save this
#	def save(self, *args, **kwargs):
#		super(UserProfile, self).save(*args, **kwargs)
#		if self.avatar:
#			size = 200, 200
#			box = (100, 100, 400, 400)
#			image = Image.open(self.avatar)
#			cropit = image.crop(box)
#			image.paste(cropit, box)
#			image.thumbnail(size, Image.ANTIALIAS)
#			fh = storage.open(self.avatar.name, "w")
#			format = 'png'  # You need to set the correct image format here
#			image.crop(box).save(fh, format)
#			fh.close()
#		else:
#			image = None

	def __unicode__(self):
		return self.user.username

	def get_avatar(self):
		thumb = str(self.avatar)
		thumb = thumb.replace('assets/', '')

		return thumb

		thumbnail = str(self.avatar_thumbnail)
		thumbnail = thumbnail.replace('assets/', '')
		return thumbnail

User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])