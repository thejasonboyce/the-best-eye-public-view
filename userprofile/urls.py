from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
			url(r'^profile/$', 'userprofile.views.user_profile'),
			url(r'^profile/(?P<user_id>\d+)/$', 'userprofile.views.view_profile'),
			url(r'^profile/(?P<user_id>\d+)/games/$', 'userprofile.views.view_usergames'),
			url(r'^profile/(?P<user_id>\d+)/follow/$', 'lobby.views.follow_user'),
			url(r'^profile/(?P<user_id>\d+)/unfollow/$', 'lobby.views.unfollow_user'),
    	    url(r'^profile/(?P<user_id>\d+)/following/$', 'userprofile.views.following'),
        	url(r'^profile/(?P<user_id>\d+)/followers/$', 'userprofile.views.followers'),
	)