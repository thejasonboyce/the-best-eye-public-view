from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from forms import UserProfileForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Q
from django.conf import settings
from django.contrib import messages
from django.template import RequestContext
from PIL import Image
from PIL import _imaging
from django.core.files.storage import default_storage as storage
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import Library
from models import UserProfile
from game.models import Game, Result
from notification.models import SiteMessage
from activity.models import Action

register = Library()


# Create your views here.

@login_required
def user_profile(request):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
	if request.method == 'POST':
		form = UserProfileForm(request.POST, request.FILES, instance=request.user.profile)
		if request.POST.get('ios_device'):
			ios_rotate = True
		else:
			ios_rotate = False
		if form.is_valid():
			f = form.save()
			return HttpResponseRedirect('/accounts/profile/')
		else:
			profile = UserProfile.objects.get(user=thisuser)

			return HttpResponseRedirect('/accounts/profile/')

	else:
		user = request.user
		profile = user.profile
		form = UserProfileForm(instance=profile)

	args = {'thisuser': thisuser, 'profile': profile, 'following': following, 'followers': followers, 's': s}
	args.update(csrf(request))

	args['form'] = form

	return render(request, 'profile.html', args)

def view_profile(request, user_id=1):
	if not request.user.is_authenticated():
		thisuser = None
		s = None
		following = None
		followers = None
	else:
		thisuser = request.user
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
		following = thisuser.user_object.following.count()
		followers = followers = UserProfile.objects.filter(following=thisuser).count()
	
	if User.objects.filter(id=user_id).exists():
		userlookup = User.objects.get(id=user_id)
		viewprofile = userlookup.profile
	else:
		userlookup = None
		viewprofile = None

	if UserProfile.objects.filter(user=thisuser, following=userlookup).exists():
		iffollowing = 1
	else:
		iffollowing = None

	if UserProfile.objects.filter(user=userlookup, following=thisuser).exists():
		follower = True
	else:
		follower = False

	args = {'thisuser': thisuser, 'userlookup': userlookup, 'viewprofile': viewprofile, 'following': following, 'followers': followers, 'iffollowing': iffollowing, 's': s, 'follower': follower}
	args.update(csrf(request))

	return render(request, 'view_profile.html', args)

def view_usergames(request, user_id=1):
	if not request.user.is_authenticated():
		thisuser = None 
		s = None
		following = None
		followers = None
	else:
		thisuser = request.user 
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
		following = thisuser.user_object.following.count()
		followers = UserProfile.objects.filter(following=thisuser).count()


	if User.objects.filter(id=user_id).exists():
		gamesuser = User.objects.get(id=user_id)
		gamesbyuser_list = Game.objects.filter(Q(createdby=gamesuser) | Q(game_host__player=gamesuser), Q(createdby=thisuser) | Q(private=thisuser) | Q(private_game=False)).order_by('-pubdate').distinct('pubdate')
	else:
		gamesuser = None
		gamesbyuser_list = None

	paginator = Paginator(gamesbyuser_list, 20)
	next = request.path

	page = request.GET.get('page')
	try:
		gamesbyuser = paginator.page(page)
	except PageNotAnInteger:
		gamesbyuser = paginator.page(1)
	except EmptyPage:
		gamesbyuser = paginator.page(paginator.num_pages)

	for game in gamesbyuser:
		game.played = Result.objects.filter(player=thisuser, game=game).exists()
		game.liked = Game.objects.filter(id=game.id, gamelikes=thisuser).exists()
		if Result.objects.filter(game=game).exists():
				game.latest = Result.objects.filter(game=game).order_by('-pubdate')[0]

	args = {'thisuser': thisuser, 'gamesuser': gamesuser, 'gamesbyuser': gamesbyuser, 'following': following, 'followers': followers, 's': s}
	return render(request, 'games_by_user.html', args)

@login_required
def following(request, user_id=1):
	thisuser = request.user
	userlookup = User.objects.get(id=user_id)
	if UserProfile.objects.filter(user=thisuser).exists():
		following_list = thisuser.user_object.following.all()
		following = thisuser.user_object.following.count()
	else:
		following = 0
		following_list = None
	followers = UserProfile.objects.filter(following=thisuser).count()
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')

	paginator = Paginator(following_list, 20)
	next = request.path

	page = request.GET.get('page')
	try:
		users_following = paginator.page(page)
	except PageNotAnInteger:
		users_following = paginator.page(1)
	except EmptyPage:
		users_following = paginator.page(paginator.num_pages)
	
	args = {'following': following, 'thisuser': thisuser, 'userlookup': userlookup, 'followers': followers, 's': s, 'users_following': users_following}
	args.update(csrf(request))

	return render(request, 'following.html', args)

@login_required
def followers(request, user_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
		followers = UserProfile.objects.filter(following=request.user).count()
	else:
		following = 0
		followers = None
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')

	args = {'thisuser': thisuser, 'followers': followers, 'following': following, 's': s}
	args['followers_list'] = []
	args.update(csrf(request))

	followers_list = UserProfile.objects.filter(following=request.user)

	paginator = Paginator(followers_list, 20)
	next = request.path

	page = request.GET.get('page')
	try:
		user_followers = paginator.page(page)
	except PageNotAnInteger:
		user_followers = paginator.page(1)
	except EmptyPage:
		user_followers = paginator.page(paginator.num_pages)
	args['user_followers'] = user_followers

	for f in followers_list:
		user_status = (f, UserProfile.objects.filter(user=thisuser, following=f.user).exists())
		args['followers_list'].append(user_status)

	return render(request, 'followers.html', args)
	