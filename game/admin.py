from django.contrib import admin
from game.models import Game, GameComment, Result, ResultComment, ResultItemComment, RandomGameItem, Achievement

# Register your models here.

admin.site.register(Game)
admin.site.register(GameComment)
admin.site.register(Result)
admin.site.register(ResultComment)
admin.site.register(ResultItemComment)
admin.site.register(RandomGameItem)
admin.site.register(Achievement)
