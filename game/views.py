from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.views.generic.base import TemplateView
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Q
from django.contrib import messages
from django.conf import settings
from django.template import RequestContext
from django.template import Library
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from datetime import date
from game.models import Game, GameComment, Result, ResultComment, ResultItemComment, RandomGameItem, Achievement
from forms import GameForm, ResultForm, GameCommentForm, ResultCommentForm, ResultItemCommentForm
from userprofile.models import UserProfile
from notification.models import SiteMessage
from activity.models import Action
from favorite.models import FavoriteGame

register = Library()

# Create your views here.

def games(request):
	if not request.user.is_authenticated():
		thisuser = None
		s = None
		followers = None
		following = None
		new_game_available = False
		games_list = Game.objects.filter(private_game=False).order_by('-pubdate')
		paginator = Paginator(games_list, 20)
		next = request.path

		page = request.GET.get('page')
		try:
			games = paginator.page(page)
		except PageNotAnInteger:
			games = paginator.page(1)
		except EmptyPage:
			games = paginator.page(paginator.num_pages)

		if request.method == "POST":
			search_text = request.POST['search_all_games']
			search_game_results = Game.objects.filter(Q(name__contains=search_text) | Q(description__contains=search_text), private_game=False).order_by('-pubdate')
			for game in search_game_results:
				if Result.objects.filter(game=game).exists():
					game.latest = Result.objects.filter(game=game).order_by('-pubdate')[0]
			search_sent = True
		else: 
			search_game_results = None
			search_sent = False

		for game in games:
			if Result.objects.filter(game=game).exists():
				game.latest = Result.objects.filter(game=game).order_by('-pubdate')[0]

	else:
		thisuser = request.user
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
		followers = UserProfile.objects.filter(following=thisuser).count()
		following = thisuser.user_object.following.count()
		new_game_available = True
		games_list = Game.objects.filter(Q(private_game=False) | Q(private=thisuser) | Q(createdby=thisuser)).order_by('-pubdate')
		paginator = Paginator(games_list, 20)
		next = request.path

		page = request.GET.get('page')
		try:
			games = paginator.page(page)
		except PageNotAnInteger:
			games = paginator.page(1)
		except EmptyPage:
			games = paginator.page(paginator.num_pages)

		for game in games:
			game.played = Result.objects.filter(game=game, player=thisuser).exists()
			game.liked = Game.objects.filter(id=game.id, gamelikes=thisuser).exists()
			game.favorite = thisuser.user_object.favorite_games.filter(id=game.id).exists()
			if Result.objects.filter(game=game).exists():
				game.latest = Result.objects.filter(game=game).order_by('-pubdate')[0]

		if request.method == "POST":
			search_text = request.POST['search_all_games']
			search_game_results = Game.objects.filter(Q(name__contains=search_text) | Q(description__contains=search_text), Q(createdby=thisuser) | Q(private=thisuser) | Q(private_game=False)).order_by('-pubdate')
			for game in search_game_results:
				game.played = Result.objects.filter(game=game, player=thisuser).exists()
				game.liked = Game.objects.filter(id=game.id, gamelikes=thisuser).exists()
				game.favorite = thisuser.user_object.favorite_games.filter(id=game.id).exists()
				if Result.objects.filter(game=game).exists():
					game.latest = Result.objects.filter(game=game).order_by('-pubdate')[0]
			search_sent = True
		else: 
			search_game_results = None
			search_sent = False

	args = {'thisuser': thisuser, 'games': games, 'following': following, 'followers': followers, 's': s, 'search_game_results': search_game_results, 'search_sent': search_sent, 'new_game_available': new_game_available}
	args.update(csrf(request))

	return render(request, 'games.html', args)

def game(request, game_id=1):
	if not request.user.is_authenticated():
		thisuser = None
		s = None
		played = False
		favorite = False
		following = None
		followers = None
	else:
		thisuser = request.user
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
		following = thisuser.user_object.following.count()
		followers = UserProfile.objects.filter(following=thisuser).count()
		if Result.objects.filter(game__id=game_id, player__id=thisuser.id).exists():
			played = Result.objects.filter(game__id=game_id, player__id=thisuser.id).order_by('pubdate')[0]
		else:
			played = False

	if Game.objects.filter(id=game_id, createdby=thisuser).exists():
		made = True
	else:
		made = False

	if Game.objects.filter(Q(private_game=False) | Q(private=thisuser) | Q(createdby=thisuser), id=game_id).exists():
		game = Game.objects.get(id=game_id)
		favorite = UserProfile.objects.filter(user=thisuser, favorite_games=game).exists()
		if game.endby_date:
			current_date = date.today()
			if game.game_closed:
				playable = False
			else:
				if current_date > game.endby_date:
					playable = False
					game.game_closed = True
					game.save()
					winning_result = Result.objects.filter(game__id=game_id).order_by('-result_points')[0]
					winning_result.gamewinner = True
					winning_result.save()
					winning_player = winning_result.player
					if not Game.objects.filter(id=game_id, createdby=thisuser).exists():
						profile_to_add_points_to = UserProfile.objects.get(user=winning_player)
						profile_to_add_points_to.totalpoints += 10
						profile_to_add_points_to.save()
					new_game_achievement = Achievement.objects.get(id=2)
					if not UserProfile.objects.filter(user=winning_player, achievements__id=2).exists():
						user_achievement = UserProfile.objects.get(user=winning_player)
						user_achievement.achievements.add(new_game_achievement)
						new_subject = "New Achievement Unlocked!"
						new_message = "A Winner Is You: For winning your first game!"
						new_achievement_notification = SiteMessage.objects.create(subject=new_subject, message=new_message, user=winning_result.player)
						add_points = profile_to_add_points_to.totalpoints
						add_points += 5
						profile_to_add_points_to.totalpoints = add_points
						profile_to_add_points_to.save()

				else:
					playable = True
		else:
			playable = True
		if GameComment.objects.filter(game=game).exists():
			comments = GameComment.objects.filter(game=game).order_by('pubdate')
			for comment in comments:
				comment.liked = GameComment.objects.filter(game=game, unique_id=comment.unique_id, gamecommentlikes=thisuser).exists()
		else:
			comments = None
		if Game.objects.filter(id=game_id, gamelikes=thisuser).exists():
			liked = True
		else:
			liked = None


	else:
		game = None
		current_date = None
		playable = False

	if Result.objects.filter(game__id=game_id).exists():
		results = Result.objects.filter(game__id=game_id).order_by('pubdate')
		latest = Result.objects.filter(game__id=game_id).order_by('-pubdate')[0]
	else:
		results = None
		latest = None

	if Game.objects.filter(id=game_id, gamelikes=thisuser).exists():
		liked = True
	else:
		liked = False

	args = {'thisuser': thisuser, 'game': game, 'played': played, 'results': results, 'made': made, 'comments': comments, 'following': following, 'followers': followers, 'liked': liked, 'playable': playable, 's': s, 'favorite': favorite, 'latest': latest}
	args.update(csrf(request))

	return render(request, 'game.html', args)

@login_required
def yourgames(request):
	thisuser = request.user
	
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
		followers = UserProfile.objects.filter(following=thisuser).count()
	else:
		following = 0
		followers = 0
	
	if Game.objects.filter(Q(createdby=thisuser) | Q(game_host__player=thisuser)).exists():
		playergames_list = Game.objects.filter(Q(createdby=thisuser) | Q(game_host__player=thisuser)).distinct('pubdate').order_by('-pubdate')
		paginator = Paginator(playergames_list, 20)
		next = request.path

		page = request.GET.get('page')
		try:
			playergames = paginator.page(page)
		except PageNotAnInteger:
			playergames = paginator.page(1)
		except EmptyPage:
			playergames = paginator.page(paginator.num_pages)

		for game in playergames:
			game.played = Result.objects.filter(game=game, player=thisuser).exists()
			game.liked = Game.objects.filter(id=game.id, gamelikes=thisuser).exists()
			game.favorite = thisuser.user_object.favorite_games.filter(id=game.id).exists()
			if Result.objects.filter(game=game).exists():
				game.latest = Result.objects.filter(game=game).order_by('-pubdate')[0]

	else:
		playergames = None
	
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')

	if request.method == "POST":
		search_text = request.POST['search_your_games']
		search_game_results = Game.objects.filter(Q(name__contains=search_text) | Q(description__contains=search_text), Q(createdby=thisuser) | Q(game_host__player=thisuser)).distinct('pubdate').order_by('-pubdate')
		for game in search_game_results:
			game.played = Result.objects.filter(game=game, player=thisuser).exists()
			game.liked = Game.objects.filter(id=game.id, gamelikes=thisuser).exists()
			game.favorite = thisuser.user_object.favorite_games.filter(id=game.id).exists()
			if Result.objects.filter(game=game).exists():
				game.latest = Result.objects.filter(game=game).order_by('-pubdate')[0]
		search_sent = True
	else: 
		search_game_results = None
		search_sent = False

	new_game_available = True

	args = {'thisuser': thisuser, 'playergames': playergames, 'following': following, 'followers': followers, 's': s, 'search_game_results': search_game_results, 'search_sent': search_sent, 'new_game_available': new_game_available}
	return render(request, 'yourgames.html', args)

@login_required
def friends_games(request):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
		followers = UserProfile.objects.filter(following=thisuser).count()
	else:
		following = None
		follwers = None
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')

	following_list = thisuser.user_object.following.all()

	friends_games_list = Game.objects.filter(Q(private_game=False) | Q(private=thisuser) | Q(createdby=thisuser), Q(createdby__in=list(following_list)) | Q(game_host__player__in=list(following_list))).order_by('-pubdate').distinct('pubdate')

	paginator = Paginator(friends_games_list, 20)
	next = request.path

	page = request.GET.get('page')
	try:
		friends_games = paginator.page(page)
	except PageNotAnInteger:
		friends_games = paginator.page(1)
	except EmptyPage:
		friends_games = paginator.page(paginator.num_pages)

	for game in friends_games:
		game.played = Result.objects.filter(game=game, player=thisuser).exists()
		game.liked = Game.objects.filter(id=game.id, gamelikes=thisuser).exists()
		game.favorite = thisuser.user_object.favorite_games.filter(id=game.id).exists()
		if Result.objects.filter(game=game).exists():
			game.latest = Result.objects.filter(game=game).order_by('-pubdate')[0]

	new_game_available = True

	if request.method == "POST":
		search_text = request.POST['search_friends_games']
		search_game_results = Game.objects.filter(Q(name__contains=search_text) | Q(description__contains=search_text), Q(createdby=thisuser) | Q(private=thisuser) | Q(private_game=False), Q(createdby__in=list(following_list)) | Q(game_host__player__in=list(following_list))).distinct('pubdate').order_by('-pubdate')
		for game in search_game_results:
			game.played = Result.objects.filter(game=game, player=thisuser).exists()
			game.liked = Game.objects.filter(id=game.id, gamelikes=thisuser).exists()
			game.favorite = thisuser.user_object.favorite_games.filter(id=game.id).exists()
			if Result.objects.filter(game=game).exists():
				game.latest = Result.objects.filter(game=game).order_by('-pubdate')[0]
		search_sent = True
	else: 
		search_game_results = None
		search_sent = False

	args = {'thisuser': thisuser, 'following': following, 'followers': followers, 's': s, 'friends_games': friends_games, 'search_game_results': search_game_results, 'search_sent': search_sent, 'new_game_available': new_game_available}

	return render(request, 'friends_games.html', args)


@login_required
def new_game(request):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
	randomgameitems = RandomGameItem.objects.all()
	if request.POST:
		form = GameForm(request.POST, request.FILES)
		if form.is_valid():
			f = form.save(commit=False)
			f.createdby = thisuser
			f.save()
			user_points = thisuser.user_object.totalpoints
			user_points += 1
			thisuser.user_object.totalpoints = user_points
			thisuser.user_object.save()

			messages.add_message(request, messages.SUCCESS, "Your Game was posted.")

			new_game_achievement = Achievement.objects.get(id=3)
			if not UserProfile.objects.filter(user=thisuser, achievements__id=3).exists():
				user_achievement = UserProfile.objects.get(user=thisuser)
				user_achievement.achievements.add(new_game_achievement)
				new_subject = "New Achievement Unlocked!"
				new_message = "Game Master: For creating your first game!"
				new_achievement_notification = SiteMessage.objects.create(subject=new_subject, message=new_message, user=thisuser)
				your_points = user_achievement.totalpoints
				your_points += 5
				user_achievement.totalpoints = your_points
				user_achievement.save()

			new_game = Game.objects.filter(createdby=thisuser).order_by('-pubdate')[0]
			if not new_game.private_game:
				new_action_item = "created a new game"
				new_url = "/games/%s" % new_game.id
				new_activity = Action.objects.create(user=thisuser, actionitem=new_action_item, url=new_url)

			return HttpResponseRedirect('/games/%s/' % new_game.id)

	else:
		form = GameForm()

	args = {'thisuser': thisuser, 'following': following, 'followers': followers, 'randomgameitems': randomgameitems, 's': s}
	args.update(csrf(request))
	
	args ['form'] = form

	return render(request, 'new_game.html', args)

@login_required
def invite_user(request, game_id=1):
	if not request.user.is_authenticated():
		thisuser = None
	else:
		thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	if Game.objects.filter(id=game_id, createdby=thisuser).exists():
		game = Game.objects.get(id=game_id)
	else:
		game = None
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
	if request.POST:
		invitation = request.POST.get('invitation')
		if User.objects.filter(username=invitation).exists():
			user_to_invite = User.objects.get(username=invitation)
			game.private.add(user_to_invite)
			messages.add_message(request, messages.SUCCESS, "Your friend was invited.")
			new_subject = "New Invitation!"
			new_message = "You were invited to join a game! Click <a href='/games/%s/'><b>here</b></a> to view it!" % game_id
			new_invite_notification = SiteMessage.objects.create(subject=new_subject, message=new_message, user=user_to_invite)
			return HttpResponseRedirect('/games/%s/invite_user' % game_id)
		else:
			messages.add_message(request, settings.DELETE_MESSAGE, "That user doesn't exist!")
			return HttpResponseRedirect('/games/%s/invite_user' % game_id)

	args = {'thisuser': thisuser, 'game': game, 'following': following, 'followers': followers, 's': s}
	args.update(csrf(request))

	return render(request, "invite_user.html", args)

@login_required
def remove_user(request, game_id=1, user_id=1):
	thisuser = request.user
	if Game.objects.filter(id=game_id, createdby=thisuser).exists():
		game = Game.objects.get(id=game_id)
		user_to_remove = User.objects.get(id=user_id)
		game.private.remove(user_to_remove)
		messages.add_message(request, settings.DELETE_MESSAGE, "This user was removed from the game.")
		return HttpResponseRedirect('/games/%s/invite_user/' % game_id)
	else:
		messages.add_message(request, settings.DELETE_MESSAGE, "This user doesn't exist!")
		return HttpResponseRedirect('/games/%s/invite_user/' % game_id)


@login_required
def edit_game(request, game_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	randomgameitems = RandomGameItem.objects.all()
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
	if not Game.objects.filter(id=game_id, createdby=thisuser).exists():
		return HttpResponseRedirect('/games/%s' % game_id)
	else:
		game = Game.objects.get(id=game_id, createdby=thisuser)
		if game.game_closed:
			return HttpResponseRedirect('/games/%s' % game_id)
		else:
			if request.POST:
				form = GameForm(request.POST, request.FILES)
				if form.is_valid():
					f = form.save(commit=False)
					f.id = game.id
					f.pubdate = game.pubdate
					f.createdby = thisuser
					if f.private_game:
						f.private_game = True
					else:
						f.private_game = False
					if f.game_thumbnail:
						pass
					else:
						f.game_thumbnail = game.game_thumbnail
					f.photocount = game.photocount
					f.item01photocount = game.item01photocount
					f.item02photocount = game.item02photocount
					f.item03photocount = game.item03photocount
					f.item04photocount = game.item04photocount
					f.save()

					return HttpResponseRedirect('/games/%s/' % game_id)

				else:
					return HttpResponseRedirect('/games/%s/' % game_id)
			
			else:
				form = GameForm(instance=game)

				args = {'thisuser': thisuser, 'game': game, 'following': following, 'followers': followers, 'randomgameitems': randomgameitems, 's': s}
				args.update(csrf(request))
					
				args['form'] = form

				return render(request, 'edit_game.html', args)

		args = {'thisuser': thisuser, 'game': game, 'following': following, 'followers': followers, 'randomgameitems': randomgameitems, 's': s}
		args.update(csrf(request))
		
		args['form'] = form
		
		return render(request, 'edit_game.html', args)

@login_required
def delete_game(request, game_id=1):
	thisuser = request.user

	if Game.objects.filter(id=game_id, createdby=thisuser).exists():
		Game.objects.filter(id=game_id, createdby=thisuser).delete()
		messages.add_message(request, settings.DELETE_MESSAGE, "Your Game was deleted.")
		return HttpResponseRedirect('/games/')

	return HttpResponseRedirect('/games/')

@login_required
def like_game(request, game_id=1):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/games/')
	else:
		thisuser = request.user

	if Game.objects.filter(id=game_id).exists():
		next = request.GET.get('next', None)
		if Game.objects.filter(id=game_id, gamelikes=thisuser).exists():
			if next:
				return redirect(next)
			else:
				return HttpResponseRedirect('/')
		else:
			g = Game.objects.get(id=game_id)
			g.gamelikes.add(thisuser)
			user_points = g.createdby.user_object.totalpoints
			user_points += 1
			g.createdby.user_object.totalpoints = user_points
			g.createdby.user_object.save()

			new_action_item = "liked a game"
			new_url = "/games/%s" % game_id
			new_activity = Action.objects.create(user=thisuser, actionitem=new_action_item, url=new_url)

			if next:
				return redirect(next)
			else:
				return HttpResponseRedirect('/')

	else:
		return HttpResponseRedirect('/games/')

@login_required
def unlike_game(request, game_id=1):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/games/')
	else:
		thisuser = request.user

	if Game.objects.filter(id=game_id, gamelikes=thisuser).exists():
		next = request.GET.get('next', None)
		g = Game.objects.get(id=game_id, gamelikes=thisuser)
		g.gamelikes.remove(thisuser)
		user_points = g.createdby.user_object.totalpoints
		user_points -= 1
		g.createdby.user_object.totalpoints = user_points
		g.createdby.user_object.save()
		
		if next:
			return redirect(next)
		else:
			return HttpResponseRedirect('/games/')

	else:
		return HttpResponseRedirect('/games/')

@login_required
def play_game(request, game_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')

	if Game.objects.filter(Q(private_game=False) | Q(private=thisuser) | Q(createdby=thisuser), id=game_id).exists():
		g = Game.objects.get(id=game_id)
		if Result.objects.filter(game__id=game_id, player=thisuser).exists():
			return HttpResponseRedirect('/games/%s/results/%s/' % (game_id, thisuser.id))
		else:
			if request.POST:
				form = ResultForm(request.POST, request.FILES)
				if form.is_valid():
					f = form.save(commit=False)
					f.player = thisuser
					f.game = g
					if f.result01crop:
						f.result_points += 1
					if f.result02crop:
						f.result_points += 1
					if f.result03crop:
						f.result_points += 1
					if f.result04crop:
						f.result_points += 3
#					if f.result05:
#						f.result_points += 1
#					if f.result06:
#						f.result_points += 1
#					if f.result07:
#						f.result_points += 1
#					if f.result08:
#						f.result_points += 1
#					if f.result09:
#						f.result_points += 1
#					if f.result10:
#						f.result_points += 1
					f.save()
					if not Game.objects.filter(id=game_id, createdby=thisuser).exists():
						latest_play = Result.objects.filter(game__id=game_id, player=thisuser).order_by('-pubdate')[0]
						user_points = thisuser.user_object.totalpoints
						user_points += 1
						if latest_play.result01:
							user_points += 1
						if latest_play.result02:
							user_points += 1
						if latest_play.result03:
							user_points += 1
						if latest_play.result04:
							user_points += 3
	#					if latest_play.result05:
	#						user_points += 1
	#					if latest_play.result06:
	#						user_points += 1
	#					if latest_play.result07:
	#						user_points += 1
	#					if latest_play.result08:
	#						user_points += 1
	#					if latest_play.result09:
	#						user_points += 1
	#					if latest_play.result10:
	#						user_points += 1
						thisuser.user_object.totalpoints = user_points
						thisuser.user_object.save()

					messages.add_message(request, messages.SUCCESS, "Your Game was posted.")

					new_game_achievement = Achievement.objects.get(id=1)
					if not UserProfile.objects.filter(user=thisuser, achievements__id=1).exists():
						user_achievement = UserProfile.objects.get(user=thisuser)
						user_achievement.achievements.add(new_game_achievement)
						new_subject = "New Achievement Unlocked!"
						new_message = "Players Gonna Play: For playing your first game!"
						new_achievement_notification = SiteMessage.objects.create(subject=new_subject, message=new_message, user=thisuser)
						your_points = user_achievement.totalpoints
						your_points += 5
						user_achievement.totalpoints = your_points
						user_achievement.save()

					if not g.private_game:
						new_action_item = "played a new game"
						new_url = "/games/%s/results/%s/" % (game_id, thisuser.id)
						new_activity = Action.objects.create(user=thisuser, actionitem=new_action_item, url=new_url)

					all_results = Result.objects.filter(game__id=game_id)
					photo_count = 0
					item01_photocount = 0
					item02_photocount = 0
					item03_photocount = 0
					item04_photocount = 0
					for result in all_results:
						if result.result01crop:
							photo_count += 1
							item01_photocount += 1
						if result.result02crop:
							photo_count += 1
							item02_photocount += 1
						if result.result03crop:
							photo_count += 1
							item03_photocount += 1
						if result.result04crop:
							photo_count += 1
							item04_photocount += 1
					g.photocount = photo_count
					g.item01photocount = item01_photocount
					g.item02photocount = item02_photocount
					g.item03photocount = item03_photocount
					g.item04photocount = item04_photocount
					g.save()

					new_subject = 'New Game Player!'
					new_message = '%s played your game, %s! <a href="/games/%s/results/%s/">Check it out</a>' % (g.createdby.username, g.name, g.id, thisuser.id)
					new_notification = SiteMessage.objects.create(subject=new_subject, message=new_message, user=g.createdby)

					return HttpResponseRedirect('/games/%s/results/%s/' % (game_id, thisuser.id))

			else:
				form = ResultForm()

		if g.item01 or g.item02:
			line1 = True
		else:
			line1 = False
		if g.item03 or g.item04:
			line2 = True
		else:
			line2 = False
		if g.item05 or g.item06:
			line3 = True
		else:
			line3 = False
		if g.item07 or g.item08:
			line4 = True
		else:
			line4 = False
		if g.item09 or g.item10:
			line5 = True
		else:
			line5 = False

		args = {'thisuser': thisuser, 'game': g, 'line1': line1, 'line2': line2, 'line3': line3, 'line4': line4, 'line5': line5, 'following': following, 'followers': followers, 's': s }
		args.update(csrf(request))

		args ['form'] = form
		return render(request, 'play_game.html', args)

def game_results_all(request, game_id=1):
	if not request.user.is_authenticated():
		thisuser = None
		s = None
	else:
		thisuser = request.user
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()

	if Game.objects.filter(Q(private_game=False) | Q(private=thisuser) | Q(createdby=thisuser), id=game_id).exists():
		if Result.objects.filter(game__id=game_id).exists():
			results = Result.objects.filter(game__id=game_id).order_by('-result_points')
		else:
			results = None
		if Game.objects.filter(id=game_id).exists():
			game = Game.objects.get(id=game_id)
		else:
			game = None
	

		args = {'thisuser': thisuser, 'results': results, 'following': following, 'followers': followers, 'game': game, 's': s}
		return render(request, 'game_results.html', args)
	else:
		return HttpResponseRedirect('/games/')

def all_photos(request, game_id=1):
	if not request.user.is_authenticated():
		thisuser = None
		s = None
		following = None
		followers = None
	else:
		thisuser = request.user
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
		following = thisuser.user_object.following.count()
		followers = UserProfile.objects.filter(following=thisuser).count()

	game = Game.objects.filter(id=game_id).order_by('-pubdate')[0]
	results_list = Result.objects.filter(game__id=game_id).order_by('-pubdate')

	paginator = Paginator(results_list, 5)
	next = request.path

	page = request.GET.get('page')
	try:
		results = paginator.page(page)
	except PageNotAnInteger:
		results = paginator.page(1)
	except EmptyPage:
		results = paginator.page(paginator.num_pages)

	for result in results:
		result.photo01liked = Result.objects.filter(id=result.id, result01likes=thisuser).exists()
		result.photo01comments = ResultItemComment.objects.filter(result=result, itemno=1).order_by('pubdate')
		for photo01comment in result.photo01comments:
			photo01comment.liked = ResultItemComment.objects.filter(id=photo01comment.id, result=result, resultitemcommentlikes=thisuser).exists()
		result.photo02liked = Result.objects.filter(id=result.id, result02likes=thisuser).exists()
		result.photo02comments = ResultItemComment.objects.filter(result=result, itemno=2).order_by('pubdate')
		for photo02comment in result.photo02comments:
			photo02comment.liked = ResultItemComment.objects.filter(id=photo02comment.id, result=result, resultitemcommentlikes=thisuser).exists()
		result.photo03liked = Result.objects.filter(id=result.id, result03likes=thisuser).exists()
		result.photo03comments = ResultItemComment.objects.filter(result=result, itemno=3).order_by('pubdate')
		for photo03comment in result.photo03comments:
			photo03comment.liked = ResultItemComment.objects.filter(id=photo03comment.id, result=result, resultitemcommentlikes=thisuser).exists()
		result.photo04liked = Result.objects.filter(id=result.id, result04likes=thisuser).exists()
		result.photo04comments = ResultItemComment.objects.filter(result=result, itemno=4).order_by('pubdate')
		for photo04comment in result.photo04comments:
			photo04comment.liked = ResultItemComment.objects.filter(id=photo04comment.id, result=result, resultitemcommentlikes=thisuser).exists()

	args = {'thisuser': thisuser, 's': s, 'following': following, 'followers': followers, 'game': game, 'results': results}
	return render(request, 'all_photos.html', args)

@login_required
def disqualify_result(request, game_id=1, user_id=1):
	thisuser = request.user
	if Result.objects.filter(game__id=game_id, player__id=user_id, game__createdby=thisuser).exists():
		game_to_disqualify = Result.objects.get(game__id=game_id, player__id=user_id)
		game_to_disqualify.disqualified = True
		game_to_disqualify.save()
		points_to_remove = game_to_disqualify.result_points
		user_to_remove_points_from = User.objects.get(id=user_id)
		user_to_remove_points_from.user_object.totalpoints -= points_to_remove
		user_to_remove_points_from.user_object.save()
		return HttpResponseRedirect('/games/%s' % game_id)

@login_required
def new_game_comment(request, game_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')

	if Game.objects.filter(Q(private_game=False) | Q(private=thisuser) | Q(createdby=thisuser), id=game_id).exists():
		game = Game.objects.filter(id=game_id).order_by('-pubdate')[0]
		if request.method == "POST":
			form = GameCommentForm(request.POST)
			if form.is_valid():
				gc = form.save(commit=False)
				gc.usertocomment = thisuser
				gc.game = Game.objects.get(id=game_id)

				if GameComment.objects.filter(game__id=game_id).exists():
					latest = GameComment.objects.filter(game__id=game_id).order_by('-pubdate')[0]
					gc_count = latest.unique_id if latest else 0
					gc_count += 1
				else:
					gc_count = 1
					
				gc.unique_id = gc_count

				form.save()
				user_points = thisuser.user_object.totalpoints
				user_points += 1
				thisuser.user_object.totalpoints = user_points
				thisuser.user_object.save()

				messages.add_message(request, messages.SUCCESS, "Your comment was posted.")

				if not gc.game.private_game:
					new_action_item = "commented on a game"
					new_url = "/games/%s" % game_id
					new_activity = Action.objects.create(user=thisuser, actionitem=new_action_item, url=new_url)

				new_subject = 'New Game Comment!'
				new_message = '%s commented on your game, %s! <a href="/games/%s/">Check it out</a>' % (gc.usertocomment.username, game.name, game.id)
				new_notification = SiteMessage.objects.create(subject=new_subject, message=new_message, user=game.createdby)

				
				return HttpResponseRedirect('/games/%s' % game_id)

			else:
				form = GameCommentForm()
				
				args = {'thisuser': thisuser, 'game': game, 'following': following, 'followers': followers, 's': s}
				args.update(csrf(request))
				
				args['form'] = form
				
				return render(request, 'new_game_comment.html', args)

def view_game_comment(request, game_id=1, comment_id=1):
	if not request.user.is_authenticated():
		thisuser = None
		s = None
	else:
		thisuser = request.user
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()

	if GameComment.objects.get(game__id=game_id, unique_id=comment_id).exists():
		comment = GameComment.objects.get(game__id=game_id, unique_id=comment_id)
	else:
		comment = None

	args = {'thisuser': thisuser, 'comment': comment, 'following': following, 'followers': followers, 's': s}
	return render(request, 'view_game_comment.html', args)

@login_required
def edit_game_comment(request, game_id=1, comment_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')

	if GameComment.objects.filter(game__id=game_id, unique_id=comment_id, usertocomment=thisuser).exists():
		game = Game.objects.get(id=game_id)
		comment = GameComment.objects.get(game__id=game_id, unique_id=comment_id, usertocomment=thisuser)
		if request.method == "POST":
			form = GameCommentForm(request.POST)
			if form.is_valid():
				gc = form.save(commit=False)
				gc.id = comment.id
				gc.usertocomment = thisuser
				gc.game = Game.objects.get(id=game_id)
				gc.pubdate = comment.pubdate
				gc.unique_id = comment.unique_id

				form.save()
				
				return HttpResponseRedirect('/games/%s' % game_id)

			else:
				form = GameCommentForm(instance=comment)
				
				args = {'thisuser': thisuser, 'game': game, 'comment': comment, 'following': following, 'followers': followers, 's': s}
				args.update(csrf(request))
				
				args['form'] = form
				
				return render(request, 'edit_game_comment.html', args)

		else:
			form = GameCommentForm(instance=comment)
				
			args = {'thisuser': thisuser, 'game': game, 'comment': comment, 'following': following, 'followers': followers, 's': s}
			args.update(csrf(request))
				
			args['form'] = form
				
			return render(request, 'edit_game_comment.html', args)

	else:

		return HttpResponseRedirect('/games/%s' % game_id)

@login_required
def delete_game_comment(request, game_id=1, comment_id=1):
	thisuser = request.user

	if GameComment.objects.filter(game__id=game_id, unique_id=comment_id, usertocomment=thisuser).exists():
		GameComment.objects.filter(game__id=game_id, unique_id=comment_id, usertocomment=thisuser).delete()
		messages.add_message(request, settings.DELETE_MESSAGE, "Your Comment was deleted.")

	return HttpResponseRedirect('/games/%s' % game_id)

@login_required
def like_game_comment(request, game_id=1, comment_id=1):
	thisuser = request.user

	if GameComment.objects.filter(game__id=game_id, unique_id=comment_id).exists():
		next = request.GET.get('next', None)
		if GameComment.objects.filter(game__id=game_id, unique_id=comment_id, gamecommentlikes=thisuser).exists():
			if next:
				return redirect(next)
			else:
				return HttpResponseRedirect('/')
		else:
			comment = GameComment.objects.get(game__id=game_id, unique_id=comment_id)
			comment.gamecommentlikes.add(thisuser)
			user_points = comment.usertocomment.user_object.totalpoints
			user_points += 1
			comment.usertocomment.user_object.totalpoints = user_points
			comment.usertocomment.user_object.save()

			if not comment.game.private_game:
				new_action_item = "liked a game comment"
				new_url = "/games/%s/" % game_id
				new_activity = Action.objects.create(user=thisuser, actionitem=new_action_item, url=new_url)

			if next:
				return redirect(next)
			else:
				return HttpResponseRedirect('/')
	else:
		return HttpResponseRedirect('/games/%s/' % game_id)

@login_required
def unlike_game_comment(request, game_id=1, comment_id=1):
	thisuser = request.user

	if GameComment.objects.filter(game__id=game_id, unique_id=comment_id, gamecommentlikes=thisuser).exists():
		comment = GameComment.objects.get(game__id=game_id, unique_id=comment_id, gamecommentlikes=thisuser)
		comment.gamecommentlikes.remove(thisuser)
		user_points = comment.usertocomment.user_object.totalpoints
		user_points -= 1
		comment.usertocomment.user_object.totalpoints = user_points
		comment.usertocomment.user_object.save()

		return HttpResponseRedirect('/games/%s/' % game_id)
	else:
		return HttpResponseRedirect('/games/%s/' % game_id)

def view_player_result(request, game_id=1, user_id=1):
	if not request.user.is_authenticated():
		thisuser = None
		s = None
		following = None
		followers = None
	else:
		thisuser = request.user
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
		following = thisuser.user_object.following.count()
		followers = UserProfile.objects.filter(following=thisuser).count()

	if Game.objects.filter(Q(private_game=False) | Q(private=thisuser) | Q(createdby=thisuser), id=game_id).exists():
		if Result.objects.filter(game__id=game_id, player__id=user_id).exists():
			result = Result.objects.filter(game__id=game_id, player__id=user_id).order_by('pubdate')[0]
			if result.result01crop or result.result02crop:
				line1 = True
			else:
				line1 = False
			if result.result03crop or result.result04crop:
				line2 = True
			else:
				line2 = False
			if result.result05 or result.result06:
				line3 = True
			else:
				line3 = False
			if result.result07 or result.result08:
				line4 = True
			else:
				line4 = False
			if result.result09 or result.result10:
				line5 = True
			else:
				line5 = False
			if Result.objects.filter(game__id=game_id, player__id=user_id, resultlikes=thisuser).exists():
				liked = True
			else:
				liked = None
			if Result.objects.filter(game__id=game_id, player__id=user_id, result01likes=thisuser).exists():
				result01liked = True
			else:
				result01liked = False
			if Result.objects.filter(game__id=game_id, player__id=user_id, result02likes=thisuser).exists():
				result02liked = True
			else:
				result02liked = False
			if Result.objects.filter(game__id=game_id, player__id=user_id, result03likes=thisuser).exists():
				result03liked = True
			else:
				result03liked = False
			if Result.objects.filter(game__id=game_id, player__id=user_id, result04likes=thisuser).exists():
				result04liked = True
			else:
				result04liked = False

		else:
			result = None
			line1 = None
			line2 = None
			line3 = None
			line4 = None
			line5 = None

		if Result.objects.filter(game__id=game_id, player=thisuser).exists():
			made = True
		else:
			made = False

		if ResultComment.objects.filter(result=result).exists():
			comments = ResultComment.objects.filter(result=result).order_by('pubdate')
			for comment in comments:
				comment.liked = ResultComment.objects.filter(result=result, unique_id=comment.unique_id, resultcommentlikes=thisuser).exists()
		else:
			comments = None
		
		if not request.user.is_authenticated():
			thisuser = None
		else:
			thisuser = request.user

		args = {'thisuser': thisuser, 'result': result, 
				'line1': line1, 'line2': line2, 'line3': line3, 'line4': line4, 'line5': line5, 
				'made': made, 'comments': comments, 's': s,
				'following': following, 'followers': followers, 'liked': liked,
				'result01liked': result01liked, 'result02liked': result02liked,
				'result03liked': result03liked, 'result04liked': result04liked}
		args.update(csrf(request))

		return render(request, 'game_result.html', args)
	else:
		return HttpResponseRedirect('/games/')

@login_required
def edit_player_result(request, game_id=1, user_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')

	if Result.objects.filter(game__id=game_id, player=thisuser).exists():
		game = Game.objects.get(id=game_id)
		player_result = Result.objects.get(game__id=game_id, player=thisuser)
		if game.item01 or game.item02:
			line1 = True
		else:
			line1 = False
		if game.item03 or game.item04:
			line2 = True
		else:
			line2 = False
		if game.item05 or game.item06:
			line3 = True
		else:
			line3 = False
		if game.item07 or game.item08:
			line4 = True
		else:
			line4 = False
		if game.item09 or game.item10:
			line5 = True
		else:
			line5 = False

		if request.POST:
			form = ResultForm(request.POST, request.FILES)
			if form.is_valid():
				r = form.save(commit=False)
				r.id = player_result.id
				r.game = player_result.game
				r.player = thisuser
				r.pubdate = player_result.pubdate
				r.location = player_result.location
				if r.result01crop:
					pass
				else:
					r.result01crop = player_result.result01crop
				if r.result02crop:
					pass
				else:
					r.result02crop = player_result.result02crop
				if r.result03crop:
					pass
				else:
					r.result03crop = player_result.result03crop
				if r.result04crop:
					pass
				else:
					r.result04crop = player_result.result04crop
				if r.result05:
					pass
				else:
					r.result05 = player_result.result05
				if r.result06:
					pass
				else:
					r.result06 = player_result.result06
				if r.result07:
					pass
				else:
					r.result07 = player_result.result07
				if r.result08:
					pass
				else:
					r.result08 = player_result.result08
				if r.result09:
					pass
				else:
					r.result09 = player_result.result09
				if r.result10:
					pass
				else:
					r.result10 = player_result.result10
				
				form.save()

				all_results = Result.objects.filter(game__id=game_id)
				photo_count = 0
				item01_photocount = 0
				item02_photocount = 0
				item03_photocount = 0
				item04_photocount = 0
				for result in all_results:
					if result.result01crop:
						photo_count += 1
						item01_photocount += 1
					if result.result02crop:
						photo_count += 1
						item02_photocount += 1
					if result.result03crop:
						photo_count += 1
						item03_photocount += 1
					if result.result04crop:
						photo_count += 1
						item04_photocount += 1
				game.photocount = photo_count
				game.item01photocount = item01_photocount
				game.item02photocount = item02_photocount
				game.item03photocount = item03_photocount
				game.item04photocount = item04_photocount
				game.save()
				
				return HttpResponseRedirect('/games/%s/results/%s/' % (game_id, user_id))

			else:
				form = ResultForm(instance=player_result)
				
				args = {'thisuser': thisuser, 'player_result': player_result, 'game': game, 'line1': line1, 'line2': line2, 'line3': line3, 'line4': line4, 'line5': line5, 'following': following, 'followers': followers, 's': s}
				args.update(csrf(request))
				
				args['form'] = form
				
				return render(request, 'edit_player_result.html', args)

		else:
			form = ResultForm(instance=player_result)
			
			args = {'thisuser': thisuser, 'player_result': player_result, 'game': game, 'line1': line1, 'line2': line2, 'line3': line3, 'line4': line4, 'line5': line5, 'following': following, 'followers': followers, 's': s}
			args.update(csrf(request))
			
			args['form'] = form
			
			return render(request, 'edit_player_result.html', args)

	else:

		return HttpResponseRedirect('/games/%s/results/%s' % (game_id, user_id))


@login_required
def delete_player_result(request, game_id=1, user_id=1):
	thisuser = request.user

	if Result.objects.filter(game__id=game_id, player=thisuser).exists():
		Result.objects.filter(game__id=game_id, player=thisuser).delete()
		messages.add_message(request, settings.DELETE_MESSAGE, "Your Result was deleted.")

	return HttpResponseRedirect('/games/%s' % game_id)

@login_required
def like_player_result(request, game_id=1, user_id=1):
	thisuser = request.user

	if Result.objects.filter(game__id=game_id, player__id=user_id).exists():
		next = request.GET.get('next', None)
		if Result.objects.filter(game__id=game_id, player__id=user_id, resultlikes=thisuser).exists():
			if next:
				return redirect(next)
			else:
				return HttpResponseRedirect('/')
		else:
			result = Result.objects.filter(game__id=game_id, player__id=user_id).order_by('-pubdate')[0]
			result.resultlikes.add(thisuser)
			user_points = result.player.user_object.totalpoints
			user_points += 1
			result.player.user_object.totalpoints = user_points
			result.player.user_object.save()
			game_points = result.result_points
			game_points += 1
			result.result_points = game_points
			result.save()

			if not result.game.private_game:
				new_action_item = "liked a game entry"
				new_url = "/games/%s/results/%s/" % (game_id, result.player.id)
				new_activity = Action.objects.create(user=thisuser, actionitem=new_action_item, url=new_url)

			if next:
				return redirect(next)
			else:
				return HttpResponseRedirect('/')

	else:
		return HttpResponseRedirect('/games/%s/results/%s' % (game_id, user_id))

	args = {'thisuser': thisuser, 'result': result}
	return HttpResponseRedirect('/games/%s/results/%s' % (game_id, user_id))

@login_required
def unlike_player_result(request, game_id=1, user_id=1):
	thisuser = request.user

	if Result.objects.filter(game__id=game_id, player__id=user_id, resultlikes=thisuser).exists():
		result = Result.objects.filter(game__id=game_id, player__id=user_id).order_by('-pubdate')[0]
		result.resultlikes.remove(thisuser)
		user_points = result.player.user_object.totalpoints
		user_points -= 1
		result.player.user_object.totalpoints = user_points
		result.player.user_object.save()
		game_points = result.result_points
		game_points -= 1
		result.result_points = game_points
		result.save()

		return HttpResponseRedirect('/games/%s/results/%s/' % (game_id, user_id))

	else:
		return HttpResponseRedirect('/games/%s/results/%s/' % (game_id, user_id))

@login_required
def like_result_item(request, game_id=1, user_id=1, item_id=1):
	thisuser = request.user

	if Result.objects.filter(game__id=game_id, player__id=user_id).exists():
		next = request.GET.get('next', None)

		if item_id == "1":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result01likes=thisuser).exists():
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				resultitem_to_like = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_like.result01likes.add(thisuser)
				user_points = resultitem_to_like.player.user_object.totalpoints
				user_points += 1
				resultitem_to_like.player.user_object.totalpoints = user_points
				resultitem_to_like.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points += 1
				resultitem_to_like.result_points = game_points
				resultitem_to_like.save()
				if not resultitem_to_like.game.private_game:
					new_action_item = "liked a photo"
					new_url = "/games/%s/results/%s/item/%s/" % (game_id, user_id, item_id)
					new_activity = Action.objects.create(user=thisuser, actionitem=new_action_item, url=new_url)
				
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
		elif item_id == "2":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result02likes=thisuser).exists():
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				resultitem_to_like = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_like.result02likes.add(thisuser)
				user_points = resultitem_to_like.player.user_object.totalpoints
				user_points += 1
				resultitem_to_like.player.user_object.totalpoints = user_points
				resultitem_to_like.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points += 1
				resultitem_to_like.result_points = game_points
				resultitem_to_like.save()
				if not resultitem_to_like.game.private_game:
					new_action_item = "liked a photo"
					new_url = "/games/%s/results/%s/item/%s/" % (game_id, user_id, item_id)
					new_activity = Action.objects.create(user=thisuser, actionitem=new_action_item, url=new_url)
				
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
		elif item_id == "3":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result03likes=thisuser).exists():
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				resultitem_to_like = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_like.result03likes.add(thisuser)
				user_points = resultitem_to_like.player.user_object.totalpoints
				user_points += 1
				resultitem_to_like.player.user_object.totalpoints = user_points
				resultitem_to_like.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points += 1
				resultitem_to_like.result_points = game_points
				resultitem_to_like.save()
				if not resultitem_to_like.game.private_game:
					new_action_item = "liked a photo"
					new_url = "/games/%s/results/%s/item/%s/" % (game_id, user_id, item_id)
					new_activity = Action.objects.create(user=thisuser, actionitem=new_action_item, url=new_url)
				
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
		elif item_id == "4":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result04likes=thisuser).exists():
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				resultitem_to_like = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_like.result04likes.add(thisuser)
				user_points = resultitem_to_like.player.user_object.totalpoints
				user_points += 1
				resultitem_to_like.player.user_object.totalpoints = user_points
				resultitem_to_like.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points += 1
				resultitem_to_like.result_points = game_points
				resultitem_to_like.save()
				if not resultitem_to_like.game.private_game:
					new_action_item = "liked a photo"
					new_url = "/games/%s/results/%s/item/%s/" % (game_id, user_id, item_id)
					new_activity = Action.objects.create(user=thisuser, actionitem=new_action_item, url=new_url)
				
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
		elif item_id == "5":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result05likes=thisuser).exists():
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				resultitem_to_like = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_like.result05likes.add(thisuser)
				user_points = resultitem_to_like.player.user_object.totalpoints
				user_points += 1
				resultitem_to_like.player.user_object.totalpoints = user_points
				resultitem_to_like.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points += 1
				resultitem_to_like.result_points = game_points
				resultitem_to_like.save()
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
		elif item_id == "6":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result06likes=thisuser).exists():
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				resultitem_to_like = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_like.result06likes.add(thisuser)
				user_points = resultitem_to_like.player.user_object.totalpoints
				user_points += 1
				resultitem_to_like.player.user_object.totalpoints = user_points
				resultitem_to_like.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points += 1
				resultitem_to_like.result_points = game_points
				resultitem_to_like.save()
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
		elif item_id == "7":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result07likes=thisuser).exists():
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				resultitem_to_like = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_like.result07likes.add(thisuser)
				user_points = resultitem_to_like.player.user_object.totalpoints
				user_points += 1
				resultitem_to_like.player.user_object.totalpoints = user_points
				resultitem_to_like.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points += 1
				resultitem_to_like.result_points = game_points
				resultitem_to_like.save()
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
		elif item_id == "8":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result08likes=thisuser).exists():
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				resultitem_to_like = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_like.result08likes.add(thisuser)
				user_points = resultitem_to_like.player.user_object.totalpoints
				user_points += 1
				resultitem_to_like.player.user_object.totalpoints = user_points
				resultitem_to_like.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points += 1
				resultitem_to_like.result_points = game_points
				resultitem_to_like.save()
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
		elif item_id == "9":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result09likes=thisuser).exists():
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				resultitem_to_like = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_like.result09likes.add(thisuser)
				user_points = resultitem_to_like.player.user_object.totalpoints
				user_points += 1
				resultitem_to_like.player.user_object.totalpoints = user_points
				resultitem_to_like.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points += 1
				resultitem_to_like.result_points = game_points
				resultitem_to_like.save()
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
		elif item_id == "10":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result10likes=thisuser).exists():
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				resultitem_to_like = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_like.result10likes.add(thisuser)
				user_points = resultitem_to_like.player.user_object.totalpoints
				user_points += 1
				resultitem_to_like.player.user_object.totalpoints = user_points
				resultitem_to_like.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points += 1
				resultitem_to_like.result_points = game_points
				resultitem_to_like.save()
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')

	else:
		return HttpResponseRedirect('/games/%s/' % game_id)

@login_required
def unlike_result_item(request, game_id=1, user_id=1, item_id=1):
	thisuser = request.user

	if Result.objects.filter(game__id=game_id, player__id=user_id).exists():
		next = request.GET.get('next', None)
		if item_id == "1":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result01likes=thisuser).exists():
				resultitem_to_unlike = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_unlike.result01likes.remove(thisuser)
				user_points = resultitem_to_unlike.player.user_object.totalpoints
				user_points -= 1
				resultitem_to_unlike.player.user_object.totalpoints = user_points
				resultitem_to_unlike.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points -= 1
				resultitem_to_unlike.result_points = game_points
				resultitem_to_unlike.save()
				
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
		elif item_id == "2":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result02likes=thisuser).exists():
				resultitem_to_unlike = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_unlike.result02likes.remove(thisuser)
				user_points = resultitem_to_unlike.player.user_object.totalpoints
				user_points -= 1
				resultitem_to_unlike.player.user_object.totalpoints = user_points
				resultitem_to_unlike.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points -= 1
				resultitem_to_unlike.result_points = game_points
				resultitem_to_unlike.save()

				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
		elif item_id == "3":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result03likes=thisuser).exists():
				resultitem_to_unlike = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_unlike.result03likes.remove(thisuser)
				user_points = resultitem_to_unlike.player.user_object.totalpoints
				user_points -= 1
				resultitem_to_unlike.player.user_object.totalpoints = user_points
				resultitem_to_unlike.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points -= 1
				resultitem_to_unlike.result_points = game_points
				resultitem_to_unlike.save()

				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
		elif item_id == "4":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result04likes=thisuser).exists():
				resultitem_to_unlike = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_unlike.result04likes.remove(thisuser)
				user_points = resultitem_to_unlike.player.user_object.totalpoints
				user_points -= 1
				resultitem_to_unlike.player.user_object.totalpoints = user_points
				resultitem_to_unlike.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points -= 1
				resultitem_to_unlike.result_points = game_points
				resultitem_to_unlike.save()

				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
		elif item_id == "5":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result05likes=thisuser).exists():
				resultitem_to_unlike = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_unlike.result05likes.remove(thisuser)
				user_points = resultitem_to_unlike.player.user_object.totalpoints
				user_points -= 1
				resultitem_to_unlike.player.user_object.totalpoints = user_points
				resultitem_to_unlike.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points -= 1
				resultitem_to_unlike.result_points = game_points
				resultitem_to_unlike.save()
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
		elif item_id == "6":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result06likes=thisuser).exists():
				resultitem_to_unlike = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_unlike.result06likes.remove(thisuser)
				user_points = resultitem_to_unlike.player.user_object.totalpoints
				user_points -= 1
				resultitem_to_unlike.player.user_object.totalpoints = user_points
				resultitem_to_unlike.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points -= 1
				resultitem_to_unlike.result_points = game_points
				resultitem_to_unlike.save()
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
		elif item_id == "7":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result07likes=thisuser).exists():
				resultitem_to_unlike = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_unlike.result07likes.remove(thisuser)
				user_points = resultitem_to_unlike.player.user_object.totalpoints
				user_points -= 1
				resultitem_to_unlike.player.user_object.totalpoints = user_points
				resultitem_to_unlike.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points -= 1
				resultitem_to_unlike.result_points = game_points
				resultitem_to_unlike.save()
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
		elif item_id == "8":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result08likes=thisuser).exists():
				resultitem_to_unlike = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_unlike.result08likes.remove(thisuser)
				user_points = resultitem_to_unlike.player.user_object.totalpoints
				user_points -= 1
				resultitem_to_unlike.player.user_object.totalpoints = user_points
				resultitem_to_unlike.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points -= 1
				resultitem_to_unlike.result_points = game_points
				resultitem_to_unlike.save()
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
		elif item_id == "9":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result09likes=thisuser).exists():
				resultitem_to_unlike = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_unlike.result09likes.remove(thisuser)
				user_points = resultitem_to_unlike.player.user_object.totalpoints
				user_points -= 1
				resultitem_to_unlike.player.user_object.totalpoints = user_points
				resultitem_to_unlike.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points -= 1
				resultitem_to_unlike.result_points = game_points
				resultitem_to_unlike.save()
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
		elif item_id == "10":
			if Result.objects.filter(game__id=game_id, player__id=user_id, result10likes=thisuser).exists():
				resultitem_to_unlike = Result.objects.get(game__id=game_id, player__id=user_id)
				resultitem_to_unlike.result10likes.remove(thisuser)
				user_points = resultitem_to_unlike.player.user_object.totalpoints
				user_points -= 1
				resultitem_to_unlike.player.user_object.totalpoints = user_points
				resultitem_to_unlike.player.user_object.save()
				game_points = resultitem_to_like.result_points
				game_points -= 1
				resultitem_to_unlike.result_points = game_points
				resultitem_to_unlike.save()
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')
			else:
				if next:
					return redirect(next)
				else:
					return HttpResponseRedirect('/')

	else:
		return HttpResponseRedirect('/games/%s/results/%s/item/%s/' % (game_id, user_id, item_id))

def view_item_results(request, game_id=1, item_id=1, user_id=1):
	if not request.user.is_authenticated():
		thisuser = None
		s = None
		followers = None
		following = None
		item_results = Result.objects.filter(game__id=game_id).order_by('-pubdate')
		for item in item_results:
			item.photo01comments = ResultItemComment.objects.filter(result=item, itemno=1).order_by('pubdate')
			item.photo02comments = ResultItemComment.objects.filter(result=item, itemno=2).order_by('pubdate')
			item.photo03comments = ResultItemComment.objects.filter(result=item, itemno=3).order_by('pubdate')
			item.photo04comments = ResultItemComment.objects.filter(result=item, itemno=4).order_by('pubdate')
		if Game.objects.filter(private_game=False, id=game_id).exists():
			game = Game.objects.get(id=game_id)
		else:
			game = None

	else:
		thisuser = request.user
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
		followers = UserProfile.objects.filter(following=thisuser).count()
		following = thisuser.user_object.following.count()

		item_results = Result.objects.filter(game__id=game_id).order_by('-pubdate')

		for item in item_results:
			item.photo01liked = Result.objects.filter(id=item.id, result01likes=thisuser).exists()
			item.photo01comments = ResultItemComment.objects.filter(result=item, itemno=1).order_by('pubdate')
			for photo01comment in item.photo01comments:
				photo01comment.liked = ResultItemComment.objects.filter(result=item, itemno=1, unique_id=photo01comment.unique_id, resultitemcommentlikes=request.user).exists()
			item.photo02liked = Result.objects.filter(id=item.id, result02likes=thisuser).exists()
			item.photo02comments = ResultItemComment.objects.filter(result=item, itemno=2).order_by('pubdate')
			for photo02comment in item.photo02comments:
				photo02comment.liked = ResultItemComment.objects.filter(result=item, itemno=2, unique_id=photo02comment.unique_id, resultitemcommentlikes=request.user).exists()
			item.photo03liked = Result.objects.filter(id=item.id, result03likes=thisuser).exists()
			item.photo03comments = ResultItemComment.objects.filter(result=item, itemno=3).order_by('pubdate')
			for photo03comment in item.photo03comments:
				photo03comment.liked = ResultItemComment.objects.filter(result=item, itemno=3, unique_id=photo03comment.unique_id, resultitemcommentlikes=request.user).exists()			
			item.photo04liked = Result.objects.filter(id=item.id, result04likes=thisuser).exists()
			item.photo04comments = ResultItemComment.objects.filter(result=item, itemno=4).order_by('pubdate')
			for photo04comment in item.photo04comments:
				photo04comment.liked = ResultItemComment.objects.filter(result=item, itemno=4, unique_id=photo04comment.unique_id, resultitemcommentlikes=request.user).exists()

		if Game.objects.filter(Q(private_game=False) | Q(private=thisuser) | Q(createdby=thisuser), id=game_id).exists():
			game = Game.objects.get(id=game_id)
		else:
			game = None

	args = {'thisuser': thisuser, 'item_results': item_results, 'item_id': item_id, 'game': game, 'following': following, 'followers': followers, 's': s}
	
	return render(request, 'view_item_results.html', args)

@login_required
def new_result_comment(request, game_id=1, user_id=1, comment_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')

	if Game.objects.filter(Q(private_game=False) | Q(private=thisuser) | Q(createdby=thisuser), id=game_id).exists():
		game = Game.objects.get(id=game_id)
		if Result.objects.filter(game__id=game_id).exists():
			result = Result.objects.get(game__id=game_id, player__id=user_id)
			if request.method == "POST":
				form = ResultCommentForm(request.POST)
				if form.is_valid():
					rc = form.save(commit=False)
					rc.usertocomment = thisuser
					rc.result = Result.objects.get(game__id=game_id, player__id=user_id)

					if ResultComment.objects.filter(result__game__id=game_id).exists():
						latest = ResultComment.objects.filter(result__game__id=game_id).order_by('-pubdate')[0]
						rc_count = latest.unique_id if latest else 0
						rc_count += 1
					else:
						rc_count = 1
						
					rc.unique_id = rc_count

					form.save()
					user_points = rc.usertocomment.user_object.totalpoints
					user_points += 1
					rc.usertocomment.user_object.totalpoints = user_points
					rc.usertocomment.user_object.save()

					messages.add_message(request, messages.SUCCESS, "Your comment was posted.")

					new_subject = 'New Result Comment!'
					new_message = '%s commented on your game result! <a href="/games/%s/results/%s/">Check it out</a>' % (rc.usertocomment.username, game.id, thisuser.id)
					new_notification = SiteMessage.objects.create(subject=new_subject, message=new_message, user=result.player)

					game = Game.objects.get(id=game_id)
					if not game.private_game:
						new_action_item = "commented on a played game"
						new_url = "/games/%s/results/%s/" % (game_id, user_id)
						new_activity = Action.objects.create(user=thisuser, actionitem=new_action_item, url=new_url)
					
					return HttpResponseRedirect('/games/%s/results/%s' % (game_id, user_id))

				else:
					form = ResultCommentForm()
					
					args = {'thisuser': thisuser, 'following': following, 'followers': followers, 's': s}
					args.update(csrf(request))
					
					args['form'] = form
					
					return render_to_response('new_result_comment.html', args)
	else:
		return HttpResponseRedirect('/games/')

def view_result_comment(request, game_id=1, user_id=1, comment_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')

	if Game.objects.filter(Q(private_game=False) | Q(private=thisuser) | Q(createdby=thisuser), id=game_id).exists():
		if ResultComment.objects.filter(result__game__id=game_id, result__player__id=user_id, unique_id=comment_id).exists():
			viewresultcomment = ResultComment.objects.get(result__game__id=game_id, result__player__id=user_id, unique_id=comment_id)
			result = Result.objects.get(game__id=game_id, player__id=user_id)
		else:
			return HttpResponseRedirect('/games/%s/results/%s' % (game_id, user_id))
	else:
		return HttpResponseRedirect('/games/')

	args = {'thisuser': thisuser, 'viewresultcommentcomment': comment, 'result': result, 'following': following, 'followers': followers, 's': s}

	return render(request, 'view_result_comment.html', args)

@login_required
def like_result_comment(request, game_id=1, user_id=1, comment_id=1):
	thisuser = request.user

	if ResultComment.objects.filter(result__game__id=game_id, result__player__id=user_id, unique_id=comment_id).exists():
		if ResultComment.objects.filter(result__game__id=game_id, result__player__id=user_id, unique_id=comment_id, resultcommentlikes=thisuser).exists():
			next = request.GET.get('next', None)
			if next:
				return redirect(next)
			else:
				return HttpResponseRedirect('/')
			
		else:
			next = request.GET.get('next', None)
			resultcommenttolike = ResultComment.objects.get(result__game__id=game_id, result__player__id=user_id, unique_id=comment_id)
			resultcommenttolike.resultcommentlikes.add(thisuser)
			user_points = resultcommenttolike.usertocomment.user_object.totalpoints
			user_points += 1
			resultcommenttolike.usertocomment.user_object.totalpoints = user_points
			resultcommenttolike.usertocomment.user_object.save()

			game = Game.objects.get(id=game_id)
			if not game.private_game:
				new_action_item = "liked a played game comment"
				new_url = "/games/%s/results/%s/" % (game_id, user_id)
				new_activity = Action.objects.create(user=thisuser, actionitem=new_action_item, url=new_url)

			if next:
				return redirect(next)
			else:
				return HttpResponseRedirect('/')

	else:

		return HttpResponseRedirect('/games/%s/results/%s' % (game_id, user_id))

@login_required
def unlike_result_comment(request, game_id=1, item_id=1, user_id=1):
	thisuser = request.user

	if ResultComment.objects.filter(result__game__id=game_id, result__player__id=user_id, resultcommentlikes=thisuser).exists():
		resultcommenttounlike = ResultComment.objects.filter(result__game__id=game_id, result__player__id=user_id, resultcommentlikes=thisuser).order_by('-pubdate')[0]
		resultcommenttounlike.resultcommentlikes.remove(thisuser)
		user_points = resultcommenttounlike.usertocomment.user_object.totalpoints
		user_points -= 1
		resultcommenttounlike.usertocomment.user_object.totalpoints = user_points
		resultcommenttounlike.usertocomment.user_object.save()

		return HttpResponseRedirect('/games/%s/items/%s/results/' % (game_id, item_id))

	else:
		return HttpResponseRedirect('/games/%s/items/%s/results/' % (game_id, item_id))

@login_required
def edit_result_comment(request, game_id=1, user_id=1, comment_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')

	if ResultComment.objects.filter(result__game__id=game_id, unique_id=comment_id, usertocomment=thisuser).exists():
		result = Result.objects.get(game__id=game_id, player__id=user_id)
		comment = ResultComment.objects.get(result__game__id=game_id, unique_id=comment_id, usertocomment=thisuser)
		if request.method == "POST":
			form = ResultCommentForm(request.POST)
			if form.is_valid():
				rc = form.save(commit=False)
				rc.id = comment.id
				rc.usertocomment = thisuser
				rc.result = Result.objects.get(game__id=game_id, player__id=user_id)
				rc.pubdate = comment.pubdate
				rc.unique_id = comment.unique_id

				form.save()
				
				return HttpResponseRedirect('/games/%s/results/%s/' % (game_id, user_id))

			else:
				form = ResultCommentForm(instance=comment)
				
				args = {'thisuser': thisuser, 'result': result, 'comment': comment}
				args.update(csrf(request))
				
				args['form'] = form
				
				return render(request, 'edit_result_comment.html', args)

		else:
			form = ResultCommentForm(instance=comment)
				
			args = {'thisuser': thisuser, 'comment': comment, 'result': result, 'following': following, 'followers': followers, 's': s}
			args.update(csrf(request))
				
			args['form'] = form
				
			return render_to_response('edit_result_comment.html', args)

	else:

		return HttpResponseRedirect('/games/%s/results/%s' % (game_id, user_id))

@login_required
def delete_result_comment(request, game_id=1, user_id=1, comment_id=1):
	thisuser = request.user

	if ResultComment.objects.filter(result__game__id=game_id, unique_id=comment_id, usertocomment=thisuser).exists():
		ResultComment.objects.filter(result__game__id=game_id, unique_id=comment_id, usertocomment=thisuser).delete()
		messages.add_message(request, settings.DELETE_MESSAGE, "Your Comment was deleted.")

	return HttpResponseRedirect('/games/%s/results/%s' % (game_id, user_id))	

def view_player_item(request, game_id=1, item_id=1, user_id=1):
	if not request.user.is_authenticated():
		thisuser = None
		s = None
		following = 0
		followers = 0
	else:
		thisuser = request.user
		following = thisuser.user_object.following.count()
		followers = UserProfile.objects.filter(following=thisuser).count()
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')

	if Game.objects.filter(Q(private_game=False) | Q(private=thisuser) | Q(createdby=thisuser), id=game_id).exists():
		game = Game.objects.get(id=game_id)
		if Result.objects.filter(game__id=game_id, player__id=user_id).exists():
			result = Result.objects.filter(game__id=game_id, player__id=user_id).order_by('-pubdate')[0]
		else:
			result = None
		if Result.objects.filter(game__id=game_id, player__id=user_id, result01likes=request.user).exists():
			result01liked = True
		else:
			result01liked = None
		if Result.objects.filter(game__id=game_id, player__id=user_id, result02likes=request.user).exists():
			result02liked = True
		else:
			result02liked = None
		if Result.objects.filter(game__id=game_id, player__id=user_id, result03likes=request.user).exists():
			result03liked = True
		else:
			result03liked = None
		if Result.objects.filter(game__id=game_id, player__id=user_id, result04likes=request.user).exists():
			result04liked = True
		else:
			result04liked = None
	else:
		game = None
		result = None

	if ResultItemComment.objects.filter(result__game__id=game_id, result__player_id=user_id, itemno=item_id).exists():
		comments = ResultItemComment.objects.filter(result__game__id=game_id, result__player__id=user_id, itemno=item_id).order_by('pubdate')
		for comment in comments:
			comment.liked = ResultItemComment.objects.filter(result__game__id=game_id, result__player__id=user_id, itemno=item_id, resultitemcommentlikes=thisuser).exists()
	else:
		comments = None
	
	args = {'thisuser': thisuser, 'result': result, 'comments': comments, 'item_id': item_id, 'game': game,
			'following': following, 'followers': followers, 's': s, 'result01liked': result01liked, 'result02liked': result02liked,
			'result03liked': result03liked, 'result04liked': result04liked}
	args.update(csrf(request))

	return render(request, 'view_player_item.html', args)


@login_required
def new_result_item_comment(request, game_id=1, item_id=1, user_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')

	if Game.objects.filter(Q(private_game=False) | Q(private=thisuser) | Q(createdby=thisuser), id=game_id).exists():
		game = Game.objects.get(id=game_id)
		if Result.objects.filter(game__id=game_id).exists():
			if request.method == "POST":
				form = ResultItemCommentForm(request.POST)
				if form.is_valid():
					ric = form.save(commit=False)
					ric.usertocomment = thisuser
					ric.result = Result.objects.get(game__id=game_id, player__id=user_id)
					ric.itemno = item_id

					if ResultItemComment.objects.filter(result__game__id=game_id, itemno=item_id).exists():
						latest = ResultItemComment.objects.filter(result__game__id=game_id, itemno=item_id).order_by('-pubdate')[0]
						ric_count = latest.unique_id if latest else 0
						ric_count += 1
					else:
						ric_count = 1
						
					ric.unique_id = ric_count

					form.save()

					user_points = ric.usertocomment.user_object.totalpoints
					user_points += 1
					ric.usertocomment.user_object.totalpoints = user_points
					ric.usertocomment.user_object.save()

					messages.add_message(request, messages.SUCCESS, "Your Comment was posted.")

					new_subject = 'New Photo Comment!'
					new_message = '%s commented on your photo! <a href="/games/%s/results/%s/item/%s/">Check it out</a>' % (ric.usertocomment.username, game_id, user_id, item_id)
					new_notification = SiteMessage.objects.create(subject=new_subject, message=new_message, user=game.createdby)

					game = Game.objects.get(id=game_id)
					if not game.private_game:
						new_action_item = "commented on a photo"
						new_url = "/games/%s/results/%s/item/%s/" % (game_id, user_id, item_id)
						new_activity = Action.objects.create(user=thisuser, actionitem=new_action_item, url=new_url)

					next = request.GET.get('next', None)
					
					if next:
						return redirect(next)
					else:
						return HttpResponseRedirect('/games/')
					
#					return HttpResponseRedirect('/games/%s/item/%s/results/%s/view/' % (game_id, item_id, user_id))

				else:
					form = ResultItemCommentForm()
					
					args = {'thisuser': thisuser, 'following': following, 'followers': followers, 's': s}
					args.update(csrf(request))
					
					args['form'] = form
					
					return render_to_response('new_result_item_comment.html', args)
	else:
		return HttpResponseRedirect('/games/')

@login_required
def edit_result_item_comment(request, game_id=1, item_id=1, user_id=1, comment_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')

	if ResultItemComment.objects.filter(result__game__id=game_id, unique_id=comment_id, usertocomment=thisuser).exists():
		result = Result.objects.get(game__id=game_id, player__id=user_id)
		comment = ResultItemComment.objects.get(result__game__id=game_id, unique_id=comment_id, usertocomment=thisuser, itemno=item_id)
		if request.method == "POST":
			form = ResultItemCommentForm(request.POST)
			if form.is_valid():
				ric = form.save(commit=False)
				ric.id = comment.id
				ric.usertocomment = thisuser
				ric.result = Result.objects.get(game__id=game_id, player__id=user_id)
				ric.pubdate = comment.pubdate
				ric.unique_id = comment.unique_id

				form.save()
				
				return HttpResponseRedirect('/games/%s/item/%s/results/%s/' % (game_id, item_id, user_id))

			else:
				form = ResultItemCommentForm(instance=comment)
				
				args = {'thisuser': thisuser, 'result': result, 'comment': comment}
				args.update(csrf(request))
				
				args['form'] = form
				
				return render(request, 'edit_result_item_comment.html', args)

		else:
			form = ResultItemCommentForm(instance=comment)
				
			args = {'thisuser': thisuser, 'comment': comment, 'result': result, 'item_id': item_id, 'following': following, 'followers': followers, 's': s}
			args.update(csrf(request))
				
			args['form'] = form
				
			return render(request, 'edit_result_item_comment.html', args)

	else:

		return HttpResponseRedirect('/games/%s/item/%s/results/%s' % (game_id, item_id, user_id))

@login_required
def delete_result_item_comment(request, game_id=1, item_id=1, user_id=1, comment_id=1):
	thisuser = request.user

	if ResultItemComment.objects.filter(result__game__id=game_id, unique_id=comment_id, usertocomment=thisuser).exists():
		ResultItemComment.objects.filter(result__game__id=game_id, unique_id=comment_id, usertocomment=thisuser).delete()
		messages.add_message(request, messages.settings.DELETE_MESSAGE, "Your Comment was deleted.")

	return HttpResponseRedirect('/games/%s/item/%s/results/%s' % (game_id, item_id, user_id))		

@login_required
def like_result_item_comment(request, game_id=1, item_id=1, user_id=1, comment_id=1):
	thisuser = request.user

	if ResultItemComment.objects.filter(result__game__id=game_id, result__player__id=user_id, unique_id=comment_id, itemno=item_id).exists():
		next = request.GET.get('next', None)
		if ResultItemComment.objects.filter(result__game__id=game_id, result__player__id=user_id, unique_id=comment_id, resultitemcommentlikes=thisuser, itemno=item_id).exists():
			if next:
				return redirect(next)
			else:
				return HttpResponseRedirect('/')
			
		else:
			resultitemcommenttolike = ResultItemComment.objects.filter(result__game__id=game_id, result__player__id=user_id, unique_id=comment_id, itemno=item_id).order_by('-pubdate')[0]
			resultitemcommenttolike.resultitemcommentlikes.add(thisuser)
			user_points = resultitemcommenttolike.usertocomment.user_object.totalpoints
			user_points += 1
			resultitemcommenttolike.usertocomment.user_object.totalpoints = user_points
			resultitemcommenttolike.usertocomment.user_object.save()

			game = Game.objects.get(id=game_id)
			if not game.private_game:
				new_action_item = "liked a photo comment"
				new_url = "/games/%s/results/%s/item/%s/" % (game_id, user_id, item_id)
				new_activity = Action.objects.create(user=thisuser, actionitem=new_action_item, url=new_url)

			if next:
				return redirect(next)
			else:
				return HttpResponseRedirect('/')

	else:

		return HttpResponseRedirect('/games/%s/item/%s/results/%s/' % (game_id, item_id, user_id))

@login_required
def unlike_result_item_comment(request, game_id=1, item_id=1, user_id=1, comment_id=1):
	thisuser = request.user

	if ResultItemComment.objects.filter(result__game__id=game_id, itemno=item_id, result__player__id=user_id, unique_id=comment_id, resultitemcommentlikes=thisuser).exists():
		next = request.GET.get('next', None)
		resultitemcommenttounlike = ResultItemComment.objects.filter(result__game__id=game_id, itemno=item_id, result__player__id=user_id, unique_id=comment_id, resultitemcommentlikes=thisuser).order_by('-pubdate')[0]
		resultitemcommenttounlike.resultitemcommentlikes.remove(thisuser)
		user_points = resultitemcommenttounlike.usertocomment.user_object.totalpoints
		user_points -= 1
		resultitemcommenttounlike.usertocomment.user_object.totalpoints = user_points
		resultitemcommenttounlike.usertocomment.user_object.save()
		if next:
			return redirect(next)
		else:
			return HttpResponseRedirect('/')

	else:
		next = request.GET.get('next', None)
		if next:
			return redirect(next)
		else:
			return HttpResponseRedirect('/')

def view_item_from_result(request, game_id=1, user_id=1, item_id=1):
	if not request.user.is_authenticated():
		thisuser = None
		s = None
		followers = None
		following = None
	else:
		thisuser = request.user
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
		followers = UserProfile.objects.filter(following=thisuser).count()
		following = thisuser.user_object.following.count()

	if request.user.is_authenticated():
		if Game.objects.filter(Q(private_game=False) | Q(private=thisuser) | Q(createdby=thisuser), id=game_id).exists():
			if Result.objects.filter(game__id=game_id, player__id=user_id).exists():
				result = Result.objects.filter(game__id=game_id, player__id=user_id).order_by('-pubdate')[0]
			else:
				result = None
			if Result.objects.filter(game__id=game_id, player__id=user_id, result01likes=request.user).exists():
				result01liked = True
			else:
				result01liked = False
			if Result.objects.filter(game__id=game_id, player__id=user_id, result02likes=request.user).exists():
				result02liked = True
			else:
				result02liked = False
			if Result.objects.filter(game__id=game_id, player__id=user_id, result03likes=request.user).exists():
				result03liked = True
			else:
				result03liked = False
			if Result.objects.filter(game__id=game_id, player__id=user_id, result04likes=request.user).exists():
				result04liked = True
			else:
				result04liked = False
		else:
			return HttpResponseRedirect('/games/')
	else:
		if Game.objects.filter(Q(private_game=False) | Q(private=thisuser) | Q(createdby=thisuser), id=game_id).exists():
			if Result.objects.filter(game__id=game_id, player__id=user_id).exists():
				result = Result.objects.filter(game__id=game_id, player__id=user_id).order_by('-pubdate')[0]
				result01liked = None
				result02liked = None
				result03liked = None
				result04liked = None
			else:
				result = None
				result01liked = None
				result02liked = None
				result03liked = None
				result04liked = None

	if ResultItemComment.objects.filter(result__game__id=game_id, result__player_id=user_id, itemno=item_id).exists():
		comments = ResultItemComment.objects.filter(result__game__id=game_id, result__player__id=user_id, itemno=item_id).order_by('pubdate')
		for comment in comments:
			comment.liked = ResultItemComment.objects.filter(result=result, itemno=item_id, resultitemcommentlikes=thisuser).exists()
	else:
		comments = None

	if Game.objects.filter(id=game_id).exists():
		game = Game.objects.get(id=game_id)
	else:
		game = None

	args = {'thisuser': thisuser, 'result': result, 'comments': comments, 'item_id': item_id, 'game': game,	'following': following, 'followers': followers, 's': s,
	'result01liked': result01liked, 'result02liked': result02liked, 'result03liked': result03liked, 'result04liked': result04liked}
	args.update(csrf(request))

	return render(request, 'view_item_from_result.html', args)

@login_required
def favorite_game(request, game_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(favorite_games__id=game_id, user=thisuser).exists():
		next = request.GET.get('next', None)
		if next:
			return redirect(next)
		else:
			return HttpResponseRedirect('/')

	else:
		game_to_favorite = Game.objects.get(id=game_id)
		user_account = UserProfile.objects.get(user=thisuser)
		user_account.favorite_games.add(game_to_favorite)
		messages.add_message(request, messages.SUCCESS, "Game added to favorites.")
		
		next = request.GET.get('next', None)
		if next:
			return redirect(next)
		else:
			return HttpResponseRedirect('/')

def view_photo_from_all_photos(request, game_id=1, user_id=1, item_id=1):
	if not request.user.is_authenticated():
		thisuser = None
		s = None
		followers = None
		following = None
	else:
		thisuser = request.user
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
		followers = UserProfile.objects.filter(following=thisuser).count()
		following = thisuser.user_object.following.count()

	if request.user.is_authenticated():
		if Game.objects.filter(Q(private_game=False) | Q(private=thisuser) | Q(createdby=thisuser), id=game_id).exists():
			if Result.objects.filter(game__id=game_id, player__id=user_id).exists():
				result = Result.objects.filter(game__id=game_id, player__id=user_id).order_by('-pubdate')[0]
			else:
				result = None
			if Result.objects.filter(game__id=game_id, player__id=user_id, result01likes=request.user).exists():
				result01liked = True
			else:
				result01liked = False
			if Result.objects.filter(game__id=game_id, player__id=user_id, result02likes=request.user).exists():
				result02liked = True
			else:
				result02liked = False
			if Result.objects.filter(game__id=game_id, player__id=user_id, result03likes=request.user).exists():
				result03liked = True
			else:
				result03liked = False
			if Result.objects.filter(game__id=game_id, player__id=user_id, result04likes=request.user).exists():
				result04liked = True
			else:
				result04liked = False
		else:
			return HttpResponseRedirect('/games/')
	else:
		if Game.objects.filter(Q(private_game=False) | Q(private=thisuser) | Q(createdby=thisuser), id=game_id).exists():
			if Result.objects.filter(game__id=game_id, player__id=user_id).exists():
				result = Result.objects.filter(game__id=game_id, player__id=user_id).order_by('-pubdate')[0]
				result01liked = None
				result02liked = None
				result03liked = None
				result04liked = None
			else:
				result = None
				result01liked = None
				result02liked = None
				result03liked = None
				result04liked = None

	if ResultItemComment.objects.filter(result__game__id=game_id, result__player_id=user_id, itemno=item_id).exists():
		comments = ResultItemComment.objects.filter(result__game__id=game_id, result__player__id=user_id, itemno=item_id).order_by('pubdate')
		for comment in comments:
			comment.liked = ResultItemComment.objects.filter(result__game__id=game_id, result__player__id=user_id, itemno=item_id, resultitemcommentlikes=thisuser).exists()
	else:
		comments = None

	if Game.objects.filter(id=game_id).exists():
		game = Game.objects.get(id=game_id)
	else:
		game = None

	args = {'thisuser': thisuser, 'result': result, 'comments': comments, 'item_id': item_id, 'game': game,	'following': following, 'followers': followers, 's': s,
	'result01liked': result01liked, 'result02liked': result02liked, 'result03liked': result03liked, 'result04liked': result04liked}
	args.update(csrf(request))

	return render(request, 'view_photo_from_all_photos.html', args)
