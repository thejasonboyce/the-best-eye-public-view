from django.shortcuts import render_to_response, redirect, render
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.csrf import csrf_exempt
from django.template import Library
from django.db.models import Q
from django.utils import timezone
from datetime import datetime
from userprofile.models import UserProfile
from django.contrib import messages
from django.conf import settings
import csv
from django.template import RequestContext
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from notification.models import SiteMessage

register = Library()

# Create your views here.

@login_required
def show_sitemessage(request, sitemessage_id=1):
	if not request.user.is_authenticated():
		thisuser = None
	else:
		thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	s = SiteMessage.objects.filter(id=sitemessage_id).order_by('-pubdate')[0]


	args = {'thisuser': thisuser, 'following': following, 'followers': followers, 's': s}

	return render(request, "notification.html", args)

@login_required
def delete_sitemessage(request, sitemessage_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	s = SiteMessage.objects.filter(id=sitemessage_id).order_by('-pubdate')[0]
	s.viewed = True
	s.save()

	return HttpResponseRedirect('/')

