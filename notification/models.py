from django.db import models
from time import time
from django.contrib.auth.models import User
from django.utils import timezone
from django.shortcuts import render_to_response
from PIL import Image
from PIL import _imaging
from django.conf import settings
from django.core.files.storage import default_storage as storage
from django.db.models.signals import post_save
from django.dispatch import receiver

def get_upload_file_name(instance, filename):
	return settings.UPLOAD_FILE_PATTERN % (str(time()).replace('.','_'), filename)

# Create your models here.

class SiteMessage(models.Model):
	subject				= models.CharField(max_length=256)
	message				= models.TextField(blank=True, null=True)
	viewed				= models.BooleanField(default=False)
	user  				= models.ForeignKey(User)
	pubdate 			= models.DateTimeField(default=timezone.now)
	
@receiver(post_save, sender=User)
def create_welcome_message(sender, **kwargs):
	if kwargs.get('created', False):
		SiteMessage.objects.create(user=kwargs.get('instance'),
									subject="Welcome to the Best Eye!",
									message="Thanks for signing up!")

	def __unicode__(self):
		return self.subject

	class Meta:
		ordering = ['-pubdate']