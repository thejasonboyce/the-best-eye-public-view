from django.conf.urls import patterns, include, url

urlpatterns = patterns('notification.views',
	url(r'^show/(?P<sitemessage_id>\d+)/$', 'show_sitemessage'),
	url(r'^delete/(?P<sitemessage_id>\d+)/$', 'delete_sitemessage'),
    )
