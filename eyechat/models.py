from django.db import models
from time import time
from django.contrib.auth.models import User
from django.utils import timezone
from django.shortcuts import render_to_response
from PIL import Image
from PIL import _imaging
from django.conf import settings
from django.core.files.storage import default_storage as storage

def get_upload_file_name(instance, filename):
	return settings.UPLOAD_FILE_PATTERN % (str(time()).replace('.','_'), filename)

# Create your models here.

class Message(models.Model):
	subject				= models.CharField(max_length=256)
	body				= models.TextField(blank=True, null=True)
	sender				= models.ForeignKey(User, related_name='sender_messages')
	recipient 			= models.ForeignKey(User, related_name='recipient_messages')
	pubdate 			= models.DateTimeField(default=timezone.now)
	sender_deleted		= models.BooleanField(default=False)
	recipient_deleted	= models.BooleanField(default=False)
	read_date			= models.DateTimeField(blank=True, null=True)

	def __unicode__(self):
		return self.subject

	class Meta:
		ordering = ['-pubdate']