from django.conf.urls import patterns, include, url

urlpatterns = patterns('eyechat.views',
	url(r'^$', 'messages_all'),
	url(r'^(?P<message_id>\d+)/$', 'get_message'),
	url(r'^new/(?P<user_id>\d+)/$', 'new_message'),
	url(r'^(?P<message_id>\d+)/delete/$', 'delete_message'),
	url(r'^(?P<message_id>\d+)/reply/$', 'reply_message'),
    )
