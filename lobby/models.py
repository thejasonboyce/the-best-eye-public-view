from django.db import models
from time import time
from django.contrib.auth.models import User
from django.utils import timezone
from django.shortcuts import render_to_response
from PIL import Image
from PIL import _imaging
from django.conf import settings
from django.core.files.storage import default_storage as storage

def get_upload_file_name(instance, filename):
	return settings.UPLOAD_FILE_PATTERN % (str(time()).replace('.','_'), filename)

# Create your models here.

class Post(models.Model):
	body = models.TextField(null=True, blank=True)
	createdby = models.ForeignKey(User, related_name='post_created_by')
	pubdate = models.DateTimeField(default=timezone.now)
	lastedit = models.DateTimeField(default=timezone.now)
	postlikes = models.ManyToManyField(User, related_name='post_likes')

	def __unicode__(self):
		return self.body

	class Meta:
		ordering = ['-pubdate']

class Comment(models.Model):
	comment_text = models.TextField(null=True, blank=True)
	createdby = models.ForeignKey(User, related_name='comment_created_by')
	pubdate = models.DateTimeField(default=timezone.now)
	id_unique = models.IntegerField(default=0)
	reply_to = models.ForeignKey(Post, related_name='post_replied_to')
	pubdate = models.DateTimeField(default=timezone.now)
	lastedit = models.DateTimeField(default=timezone.now)
	commentlikes = models.ManyToManyField(User, related_name='comment_likes')

	def __unicode__(self):
		return self.comment_text

	class Meta:
		ordering = ['pubdate']