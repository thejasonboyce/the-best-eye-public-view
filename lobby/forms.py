from django import forms
from models import Post, Comment
from django.contrib.auth.models import User

class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('body',)
        widgets = {
        	'body': forms.Textarea(attrs={'rows': '3'}),
        }

class CommentForm(forms.ModelForm):

	class Meta:
		model = Comment
		fields = ('comment_text',)

class PostEditForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('body',)
        widgets = {
        	'body': forms.Textarea(attrs={'rows': '3'}),
        }