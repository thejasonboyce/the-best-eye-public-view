from django.shortcuts import render_to_response, redirect, render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.core.context_processors import csrf
from django.utils import timezone
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.csrf import csrf_exempt
from django.template import Library
from django.db.models import Q
from django.utils import timezone
from django.conf import settings
from django.contrib import messages
from django.template import RequestContext
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import Library
from django.contrib.auth.decorators import login_required
import csv
from datetime import datetime
from forms import PostForm, CommentForm, PostEditForm
from lobby.models import Post, Comment
from userprofile.models import UserProfile
from activity.models import Action
from notification.models import SiteMessage

register = Library()

# Create your views here.

def home(request):
	if not request.user.is_authenticated():
		thisuser = None 
		followers = None
		s = None
		new_post_available = False
	else:
		thisuser = request.user 
		followers = UserProfile.objects.filter(following=thisuser).count()
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
		new_post_available = True

	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0

	post_list = Post.objects.all()
	paginator = Paginator(post_list, 20)
	next = request.path

	page = request.GET.get('page')
	try:
		posts = paginator.page(page)
	except PageNotAnInteger:
		posts = paginator.page(1)
	except EmptyPage:
		posts = paginator.page(paginator.num_pages)

	for post in posts:
		post.liked = Post.objects.filter(id=post.id, postlikes=thisuser).exists()
		post.comments = post.post_replied_to.all()
		for comment in post.comments:
			comment.liked = Comment.objects.filter(id=comment.id, commentlikes=thisuser).exists()

	if request.method == "POST":
		search_text = request.POST['search_lobby_posts']
		search_lobby_results = Post.objects.filter(body__contains=search_text).order_by('-pubdate')
		for post in search_lobby_results:
			post.liked = Post.objects.filter(id=post.id, postlikes=thisuser).exists()
			post.comments = post.post_replied_to.all()
			for comment in post.comments:
				comment.liked = Comment.objects.filter(id=comment.id, commentlikes=thisuser).exists()
		search_sent = True
	else: 
		search_lobby_results = None
		search_sent = False

	args = {'posts': posts, 'thisuser': thisuser, 'following': following, 'followers': followers, 'search_sent': search_sent, 'search_lobby_results': search_lobby_results, 's': s, 'new_post_available': new_post_available}
	args.update(csrf(request))

	return render(request, 'lobby.html', args)

@login_required
def new_post(request):
	thisuser = request.user
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()

	if request.method == "POST":
		form = PostForm(request.POST)
		if form.is_valid():
			p = form.save(commit=False)
			p.createdby = thisuser
			p.save()
			user_points = thisuser.user_object.totalpoints
			user_points += 1
			thisuser.user_object.totalpoints = user_points
			thisuser.user_object.save()

			messages.add_message(request, messages.SUCCESS, "Your Post was posted.")

			latest_post = Post.objects.filter(createdby=thisuser).order_by('-pubdate')[0]
			new_action_item = "made a new post in the lobby"
			new_url = "/lobby/posts/%s/" % latest_post.id
			new_activity = Action.objects.create(user=thisuser, actionitem=new_action_item, url=new_url)
			
			return HttpResponseRedirect('/lobby/')
	else:
		form = PostForm()
		
		args = {'thisuser': thisuser, 'following': following, 'followers': followers, 's': s}
		args.update(csrf(request))
		
		args['form'] = form
		
		return render(request, 'new_post.html', args)

def post(request, post_id=1):
	if not request.user.is_authenticated():
		thisuser = None 
		followers = None
		s = None
	else:
		thisuser = request.user 
		followers = UserProfile.objects.filter(following=thisuser).count()
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')

	if Post.objects.filter(id=post_id).exists():
		post = Post.objects.get(id=post_id)
		if Post.objects.filter(id=post_id, postlikes=thisuser).exists():
			post.liked = True
	else:
		post = None

	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0

	if Comment.objects.filter(reply_to__id=post_id).exists():
		comments = Comment.objects.filter(reply_to__id=post_id).order_by('pubdate')
		for comment in comments:
			comment.liked = Comment.objects.filter(id=comment.id, commentlikes=thisuser).exists()
	else:
		comments = None

	args = {'post': post, 'thisuser': thisuser, 'following': following, 'followers': followers, 's': s, 'comments': comments}
	args.update(csrf(request))

	return render(request, 'post.html', args)

@login_required
def like_post(request, post_id=1):
	thisuser = request.user

	if Post.objects.filter(id=post_id).exists():
		next = request.GET.get('next', None)
		if Post.objects.filter(id=post_id, postlikes=thisuser).exists():
			if next:
				return redirect(next)
			else:
				return HttpResponseRedirect('/')
		else:
			post_to_like = Post.objects.get(id=post_id)
			post_to_like.postlikes.add(thisuser)
			user_points = post_to_like.createdby.user_object.totalpoints
			user_points += 1
			post_to_like.createdby.user_object.totalpoints = user_points
			post_to_like.createdby.user_object.save()

			new_action_item = "liked a post in the lobby"
			new_url = "/lobby/posts/%s/" % post_id
			new_activity = Action.objects.create(user=thisuser, actionitem=new_action_item, url=new_url)

			if next:
				return redirect(next)
			else:
				return HttpResponseRedirect('/')

@login_required
def unlike_post(request, post_id=1):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/lobby/')
	else:
		thisuser = request.user

	if Post.objects.filter(id=post_id, postlikes=thisuser).exists():
		next = request.GET.get('next', None)
		p = Post.objects.get(id=post_id, postlikes=thisuser)
		p.postlikes.remove(thisuser)
		user_points = p.createdby.user_object.totalpoints
		user_points -= 1
		p.createdby.user_object.totalpoints = user_points
		p.createdby.user_object.save()
		
		if next:
			return redirect(next)
		else:
			return HttpResponseRedirect('/lobby/')

	else:
		return HttpResponseRedirect('/lobby/')

@login_required
def edit_post(request, post_id=1):
	thisuser = request.user
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()

	if Post.objects.filter(id=post_id, createdby=thisuser).exists():
		post = Post.objects.get(id=post_id)
	else:
		post = None

	if request.method == "POST":
		if Post.objects.filter(id=post_id, createdby=thisuser).exists():
			form = PostEditForm(request.POST)
			if form.is_valid():
				p = form.save(commit=False)
				p.id = post.id
				p.createdby = thisuser
				p.pubdate = p.pubdate
				form.save()
				
				return HttpResponseRedirect('/lobby/post/%s/' % post_id)
		else:
			return HttpResponseRedirect('/lobby/')
	else:
		form = PostEditForm(instance=post)
		
		args = {'thisuser': thisuser, 'post': post, 'following': following, 'followers': followers, 's': s}
		args.update(csrf(request))
		
		args['form'] = form
		
		return render(request, 'edit_post.html', args)

@login_required
def delete_post(request, post_id=1):
	thisuser = request.user

	if Post.objects.filter(id=post_id, createdby=thisuser).exists():
		Post.objects.filter(id=post_id, createdby=thisuser).delete()
		messages.add_message(request, settings.DELETE_MESSAGE, "Your Post was deleted.")
		return HttpResponseRedirect('/lobby/')

	return HttpResponseRedirect('/lobby/')

@login_required
def new_comment(request, post_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	post = Post.objects.get(id=post_id)

	if request.method == "POST":
		form = CommentForm(request.POST)
		if form.is_valid():
			c = form.save(commit=False)
			c.createdby = thisuser
			c.reply_to = post

			if Comment.objects.filter(reply_to=post_id).exists():
				latest = Comment.objects.filter(reply_to=post_id).order_by('-pubdate')[0]
				c_count = latest.id_unique if latest else 0
				c_count += 1
			else:
				c_count = 1
				
			c.id_unique = c_count

			form.save()
			user_points = thisuser.user_object.totalpoints
			user_points += 1
			thisuser.user_object.totalpoints = user_points
			thisuser.user_object.save()

			new_action_item = "commented on a post in the lobby"
			new_url = "/lobby/posts/%s/" % post_id
			new_activity = Action.objects.create(user=thisuser, actionitem=new_action_item, url=new_url)

			messages.add_message(request, messages.SUCCESS, "Your Comment was posted.")

			new_subject = 'New Post Comment!'
			new_message = '%s commented on your post. <a href="/lobby/posts/%s/">Check it out</a>' % (c.createdby, post_id)
			new_notification = SiteMessage.objects.create(subject=new_subject, message=new_message, user=post.createdby)
			
			return HttpResponseRedirect('/lobby/posts/%s/' % post_id)
	else:
		form = CommentForm()
		
		args = {'thisuser': thisuser, 'post': post, 'following': following, 'followers': followers}
		args.update(csrf(request))
		
		args['form'] = form
		
		return render(request, 'new_comment.html', args)

@login_required
def like_comment(request, post_id=1, comment_id=1):
	thisuser = request.user
	
	if Comment.objects.filter(id_unique=comment_id, reply_to__id=post_id).exists():
		next = request.GET.get('next', None)
		if Comment.objects.filter(id_unique=comment_id, reply_to__id=post_id, commentlikes=thisuser).exists():
			if next:
				return redirect(next)
			else:
				return HttpResponseRedirect('/')
		else:
			comment = Comment.objects.get(reply_to__id=post_id, id_unique=comment_id)
			comment.commentlikes.add(thisuser)
			user_points = comment.createdby.user_object.totalpoints
			user_points += 1
			comment.createdby.user_object.totalpoints = user_points
			comment.createdby.user_object.save()

			new_action_item = "liked a comment on a post"
			new_url = "/lobby/posts/%s/" % post_id
			new_activity = Action.objects.create(user=thisuser, actionitem=new_action_item, url=new_url)

			if next:
				return redirect(next)
			else:
				return HttpResponseRedirect('/')
	else:
		return HttpResponseRedirect('/lobby/posts/%s' % post_id)

@login_required
def unlike_comment(request, post_id=1, comment_id=1):
	thisuser = request.user

	if Comment.objects.filter(id_unique=comment_id, reply_to__id=post_id, commentlikes=thisuser).exists():
		next = request.GET.get('next', None)
		c = Comment.objects.get(id_unique=comment_id, reply_to__id=post_id, commentlikes=thisuser)
		c.commentlikes.remove(thisuser)
		user_points = c.createdby.user_object.totalpoints
		user_points -= 1
		c.createdby.user_object.totalpoints = user_points
		c.createdby.user_object.save()
		
		if next:
			return redirect(next)
		else:
			return HttpResponseRedirect('/lobby/')

	else:
		return HttpResponseRedirect('/lobby/')

@login_required
def edit_comment(request, post_id=1, comment_id=1):
	thisuser = request.user
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	post = Post.objects.get(id=post_id)
	comment = Comment.objects.get(reply_to__id=post_id, id_unique=comment_id)

	if request.method == "POST":
		if Comment.objects.filter(reply_to__id=post_id, id_unique=comment_id, createdby=thisuser).exists():
			form = CommentForm(request.POST)
			if form.is_valid():
				c = form.save(commit=False)
				c.createdby = thisuser
				c.reply_to = post
				c.pubdate = comment.pubdate
				c.id = comment.id
				c.id_unique = comment.id_unique
				form.save()
				
				return HttpResponseRedirect('/lobby/posts/%s/' % post_id)
		else:
			return HttpResponseRedirect('/lobby/')
	else:
		form = CommentForm(instance=comment)
		
		args = {'thisuser': thisuser, 'post': post, 'comment': comment, 'following': following, 'followers': followers, 's': s}
		args.update(csrf(request))
		
		args['form'] = form
		
		return render(request, 'edit_comment.html', args)

@login_required
def delete_comment(request, post_id=1, comment_id=1):
	thisuser = request.user

	if Comment.objects.filter(reply_to__id=post_id, id_unique=comment_id, createdby=thisuser).exists():
		Comment.objects.filter(reply_to__id=post_id, id_unique=comment_id, createdby=thisuser).delete()
		messages.add_message(request, settings.DELETE_MESSAGE, "Your Comment was deleted.")
		return HttpResponseRedirect('/lobby/')

	return HttpResponseRedirect('/lobby/')

@login_required
def follow_user(request, user_id=1):
	thisuser = request.user
	usertofollow = User.objects.get(id=user_id)
	following = thisuser
	followers = UserProfile.objects.filter(following=thisuser).count()
	
	if UserProfile.objects.filter(user=thisuser, following=usertofollow).exists():
		nextpage = request.GET.get('nextpage', None)
		if nextpage:
			return redirect(nextpage)
		else:
			return HttpResponseRedirect('/')

	else:
		followuser, created = UserProfile.objects.get_or_create(user=thisuser)
		followuser.following.add(usertofollow)

		new_action_item = "started following %s" % usertofollow.username
		new_url = "/accounts/profile/%s/" % usertofollow.id 
		new_activity = Action.objects.create(user=thisuser, actionitem=new_action_item, url=new_url)

		nextpage = request.GET.get('nextpage', None)
		if nextpage:
			return redirect(nextpage)
		else:
			return HttpResponseRedirect('/')

@login_required
def unfollow_user(request, user_id=1):
	thisuser = request.user
	usertounfollow = User.objects.get(id=user_id)
	following = thisuser
	followers = UserProfile.objects.filter(following=thisuser).count()

	if UserProfile.objects.filter(user=thisuser, following=usertounfollow).exists():
		removefollower = UserProfile.objects.get(user=thisuser, following=usertounfollow)
		removefollower.following.remove(usertounfollow)

		nextpage = request.GET.get('nextpage', None)
		if nextpage:
			return redirect(nextpage)
		else:
			return HttpResponseRedirect('/')

	else:
		nextpage = request.GET.get('nextpage', None)
		if nextpage:
			return redirect(nextpage)
		else:
			return HttpResponseRedirect('/')

